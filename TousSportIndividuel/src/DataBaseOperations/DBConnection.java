/**
 * 
 */
package DataBaseOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

/**
 * @author Touka
 * This class is used to establish the connection to the database
 */
public class DBConnection {
	
	
	private static String url = "jdbc:mysql://mysql-projetintegration.alwaysdata.net:3306/projetintegration_2018";
	private static String user = "174903";
	private static String passwd = "cergyprojet";
	
	public static Connection connectToDB() {
		Connection conn = null ;
		try {  
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url, user, passwd);
		    
		} catch (SQLException e) {
			System.err.println("Failed to connect to database");
			JOptionPane.showMessageDialog(null, 
                    "PLEASE VERIFY YOUR INTERNET CONNECTION", 
                    "WARNING : CONNETION LOST", 
                    JOptionPane.WARNING_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return conn;
	}

}
