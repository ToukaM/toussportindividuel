/**
 * 
 */
package userData;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import utils.PasswordUtils;

/**
 * @author Touka
 * User class contains all the informations about each user
 */
public class User {
	private int idUser;
	private String firstName;
	private String lastName;
	private int age;
	private String sexe;
	private String mail;
	private String password;
	private ArrayList<Integer> friendList;
	private ArrayList<String> nameFriendList;
	private ArrayList<String> sports;
	public User(int idUser, String firstName, String lastName, int age, String sexe, String mail, String password,
			ArrayList<Integer> friendList,ArrayList<String> nameFriendList, ArrayList<String> sports) {
		super();
		this.idUser = idUser;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.sexe = sexe;
		this.mail = mail;
		this.password = password;
		this.friendList = friendList;
		this.sports = sports;
	}
	public User() {
		super();
	}
	public User(int idUser) {
		super();
		this.idUser = idUser;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase();
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName.substring(0, 1).toUpperCase() + lastName.substring(1).toLowerCase();
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ArrayList<Integer> getFriendList() {
		return friendList;
	}
	public ArrayList<String> getNameFriendList() {
		return nameFriendList;
	}
	public void setNameFriendList(ArrayList<String> nameFriendList) {
		this.nameFriendList = nameFriendList;
	}
	public void setFriendList(ArrayList<Integer> friendList) {
		this.friendList = friendList;
	}
	public ArrayList<String> getSports() {
		return sports;
	}
	public void setSports(ArrayList<String> sports) {
		this.sports = sports;
	}
	
	public void registerIntoDB(String salt) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = this.getFirstName()+"','"+this.getLastName()+"',"+this.getAge()+",'"+this.getSexe()+"','"+this.getMail()+"','"+this.getPassword()+"','"+salt+"')";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO User (firstName,lastName,age,sexe,mail,password,salt) VALUES ('"+values+"");
	      ps1.executeUpdate();
	      PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE mail='"+this.getMail()+"'");
	      ResultSet rs = ps.executeQuery();
	      int newID=0;
	      while(rs.next()){
	    	  newID=rs.getInt("idUser");
	      }
	      this.setIdUser(newID);
	      addToSportTable(newID);
	      conn.close();
	}
	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
				+ ", sexe=" + sexe + ", mail=" + mail + ", password=" + password + ", friendList=" + friendList
				+ ", sports=" + sports + "]";
	}
	
	public boolean verifyEmail(String mail) throws SQLException {
		boolean exist = false;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE mail='"+mail+"'");
	    ResultSet rs = ps.executeQuery();
	    int i=0;
	    while(rs.next()){
	    	  i++;
	    }
	    if(i!=0) {
	    	exist = true;
	    }
		conn.close();
		return exist;
	}
	
	public void signUp(String firstName,String lastName, String gender, String mail, String password) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setMail(mail);
		user.setSexe(gender);
		String salt = PasswordUtils.getSalt(30);
		String mySecurePassword = PasswordUtils.generateSecurePassword(password, salt);
		user.setPassword(mySecurePassword);
		try {
			user.registerIntoDB(salt);
			
		} catch (SQLException e) {
			System.err.println("Unable to register in the database");
			e.printStackTrace();
		}
	}
	public void addToSportTable(int idUser) throws SQLException{
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO Sports (idUser,Bowling,Jogging,LongJump,ShotPut,Strengthening,Swimming) VALUES ("+idUser+",false,false,false,false,false,false)");
	    ps1.executeUpdate();
		conn.close();
	}
	public void addSport(String sport) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("UPDATE Sports SET "+sport+" = "+true+" WHERE idUser = "+this.idUser+"");
		ps.executeUpdate();
		this.fetchSports();
		conn.close();
	}
	public void fetchSports() throws SQLException{
		ArrayList<String> sportList = new ArrayList<String>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT * FROM Sports WHERE idUser = "+idUser);
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()){
	    	if(rs.getBoolean("Bowling")==true) {
	    		sportList.add("Bowling");
	    	}
	    	if(rs.getBoolean("Jogging")==true) {
	    		sportList.add("Jogging");
	    	}
	    	if(rs.getBoolean("LongJump")==true) {
	    		sportList.add("LongJump");
	    	}
	    	if(rs.getBoolean("ShotPut")==true) {
	    		sportList.add("ShotPut");
	    	}
	    	if(rs.getBoolean("Strengthening")==true) {
	    		sportList.add("Strengthening");
	    	}
	    	if(rs.getBoolean("Swimming")==true) {
	    		sportList.add("Swimming");
	    	}
	    }
		conn.close();
		this.setSports(sportList);
	}
	public void fetchFriends() throws SQLException{
		ArrayList<Integer> friends = new ArrayList<Integer>();
		ArrayList<String> nameFriends = new ArrayList<String>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT DISTINCT idUser,friends,firstName,lastName FROM FriendShip f, User u WHERE f.sender = "+this.idUser+" AND f.receiver = u.idUser OR f.receiver = "+this.idUser+" AND f.sender = u.idUser");
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
	    	if(rs.getBoolean("friends")==true) {
	    		friends.add(rs.getInt("idUser"));
	    		nameFriends.add(rs.getString("firstName")+" "+rs.getString("lastName"));
	    	}
	    }
		conn.close();
		this.setFriendList(friends);
		this.setNameFriendList(nameFriends);
	}
	public void sendFriendRequest(int idUser) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO FriendShip (sender,receiver,friends) VALUES ("+this.idUser+","+idUser+","+false+")");
	    ps1.executeUpdate();
		conn.close();
	}
	public ArrayList<Integer> fetchFriendRequests() throws SQLException {
		ArrayList<Integer> friendRequests = new ArrayList<Integer>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT sender FROM FriendShip WHERE receiver = "+this.idUser+" AND friends = "+false+"");
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
	    	friendRequests.add(rs.getInt("sender"));
	    }
		conn.close();
		return friendRequests;
	}
	public void acceptFriendRequest(int idUser) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE FriendShip SET friends = "+true+" WHERE receiver = "+this.idUser+" AND sender = "+idUser+"");
	    ps1.executeUpdate();
	    this.fetchFriends();
		conn.close();
	}
	
	public boolean verifyFriendShip(int idUser,int friendUser) throws SQLException {
		boolean accepted = false;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT friends FROM FriendShip WHERE receiver = "+idUser+" AND sender = "+friendUser+" OR receiver = "+friendUser+" AND sender = "+idUser+" ");
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
	    	accepted = rs.getBoolean("friends");
	    }
		conn.close();
		return accepted;
	}
	
	public boolean verifyFriendRequest(int idUser, int friendUser) throws SQLException{
		boolean sent = false;
		int increment=0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT friends FROM FriendShip WHERE receiver = "+friendUser+" AND sender = "+idUser+" ");
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
	    	increment++;
	    }
		conn.close();
		if(increment > 0) {
			sent = true;
		}
		return sent;
	}
	    
	public void login(String mail, String password) throws SQLException {
		String passwordToVerify = null ;
		String salt = null;
		boolean verified = false;
		boolean exist = false;
		exist = verifyEmail(mail);
		if(exist==true) {
			Connection conn = DBConnection.connectToDB();
			PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE mail='"+mail+"'");
		    ResultSet rs = ps.executeQuery();
		    while(rs.next()){
		    	passwordToVerify = rs.getString("password");
		    	salt = rs.getString("salt");
		    }
		    verified = PasswordUtils.verifyUserPassword(password, passwordToVerify, salt);
		    if(verified==true) {
			    ResultSet rs1 = ps.executeQuery();
			    while(rs1.next()){
			    	this.setIdUser(rs1.getInt("idUser"));
			    	this.setFirstName(rs1.getString("firstName"));
			    	this.setLastName(rs1.getString("lastName"));
			    	this.setAge(rs1.getInt("age"));
			    	this.setMail(rs1.getString("mail"));
			    	this.setSexe(rs1.getString("sexe"));
			    }
			    this.fetchSports();
			    this.fetchFriends();
		    }
			conn.close();
		}
	}
	public String updateUserData(User user,String firstName, String lastName, int age, String sexe, String oldPassword, String newPassword) throws SQLException {
		String message;
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAge(age);
		user.setSexe(sexe);
		message ="<html> Changes successfully <br> executed ";
		
		Connection conn = DBConnection.connectToDB();
		if (verifyPassword(user, oldPassword, conn)) {
			
			String salt = PasswordUtils.getSalt(30);
			String mySecurePassword = PasswordUtils.generateSecurePassword(newPassword, salt);
			user.setPassword(mySecurePassword);
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement("UPDATE User SET firstName='"+firstName+"', lastName='"+lastName+"', age="+age+","
					+ " sexe='"+sexe+"', password='"+mySecurePassword+"', salt='"+salt+"' WHERE idUSer="+idUser+"");
			
			message = message+"and <font color=green>password <br> is changed</font></html>";
			ps.executeUpdate();
		}
		else {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement("UPDATE User SET firstName='"+firstName+"', lastName='"+lastName+"', age="+age+","
					+ " sexe='"+sexe+"'  WHERE idUser="+idUser+""); 
			message = message+"but <font color=red>password <br> is not changed</html>";
			ps.executeUpdate();
		}
		
			
		
		conn.close();
		return message;
	}

	
	public boolean verifyPassword(User user,String password, Connection conn) throws SQLException{
		
		String passwordToVerify = null ;
		String salt = null;
		boolean verified = false;
		
		int idUser =user.getIdUser();
		
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("SELECT password,salt FROM User WHERE idUser="+idUser+"");
	    ResultSet rs = ps1.executeQuery();
	    while(rs.next()){
	    salt = rs.getString("salt");
	    passwordToVerify = rs.getString("password");
	    }
	   
	    verified = PasswordUtils.verifyUserPassword(password, passwordToVerify, salt);
	    if(verified==true) {
	    	return true;
	    }
	    else return false;
	}
	public User fetchUserData(int idUser) throws SQLException {
		User user = new User();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			user.setIdUser(rs.getInt("idUser"));
			user.setFirstName(rs.getString("firstName"));
			user.setLastName(rs.getString("lastName"));
			user.setAge(rs.getInt("age"));
			user.setSexe(rs.getString("sexe"));
			user.fetchSports();
		}
		conn.close();
		return user;
	}
	
	public int fetchUserID(String firstName, String lastName) throws SQLException {
		int userID=0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE firstName like '"+firstName+"' AND lastName like '"+lastName+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			userID = rs.getInt("IdUser");
		}
		conn.close();
		return userID;
	}
	
	public ArrayList<Integer> searchUser(String entry) throws SQLException{
		ArrayList<Integer> users = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE firstName like '%"+entry+"%' OR lastName like '%"+entry+"%'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int userID = 0;
			userID = rs.getInt("idUser");
			users.add(userID);
		}
		conn.close();
		return users;
	}
	
}
