package sportData;
/**
 * 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import userData.User;

/**
 * @author Touka
 * Strenghening class defines the attributs of Strenghening sport
 */
public class Strengthening implements Sport{
	
	private int idStrenghening; 
	private int number; //Number of movements
	private String typeMovement; //Type of movement 
	private String date; // Date of the session
	public Strengthening(int idStrenghening, int number, String typeMovement, String date) {
		super();
		this.idStrenghening = idStrenghening;
		this.number = number;
		this.typeMovement = typeMovement;
		this.date = date;
	}
	public Strengthening() {
		super();
	}
	public int getIdStrenghening() {
		return idStrenghening;
	}
	public void setIdStrenghening(int idStrenghening) {
		this.idStrenghening = idStrenghening;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTypeMovement() {
		return typeMovement;
	}
	public void setTypeMovement(String typeMovement) {
		this.typeMovement = typeMovement;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void registerIntoDB(User user,int number,String typeMovement, String date) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = number+",'"+typeMovement+"','"+date+"',"+user.getIdUser()+")";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO Strengthening (number,movementType,date,idUser) VALUES ("+values+"");
	      int rs1 = ps1.executeUpdate();
	      conn.close();
	}
	
	public ArrayList<Strengthening> fetchStrengtheningData(int idUser) throws SQLException {
		ArrayList<Strengthening> strengtheningData = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Strengthening WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Strengthening strengthening = new Strengthening();
			strengthening.setIdStrenghening(rs.getInt("idStrengthening"));
			strengthening.setNumber(rs.getInt("number"));
			strengthening.setTypeMovement(rs.getString("movementType"));
			strengthening.setDate(rs.getString("date"));
			strengtheningData.add(strengthening);
		}
		conn.close();
		return strengtheningData;
	}
	public int getTotalMovesNumber(int idUser, String typeMovement) throws SQLException {
		int totalMoves = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT number FROM Strengthening WHERE idUser = "+idUser+" AND movementType ='"+typeMovement+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			totalMoves = totalMoves + rs.getInt("number");
		}
		conn.close();
		return totalMoves;
	}
	
	protected int scoreAchievement(int idUser) throws SQLException {
		int score = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT SUM(number) FROM Strengthening WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		rs.next();
		if(rs.getString(1) != null) {
			score = Integer.parseInt(rs.getString(1));
		}
		conn.close();
		return score;
	}
	public int levelAchievementSt(int idUser) throws SQLException {
		int achievementLevel = 0;
		int score = 0, requierdScore1 = 2000, requierdScore2 = 7000, requierdScore3 = 20000;
		score = scoreAchievement(idUser);
		if(score >= requierdScore1 && score < requierdScore2) {
			achievementLevel = 1;
		} 
		else if(score >= requierdScore2 && score < requierdScore3) {
			achievementLevel = 2;
		}
		else if(score >= requierdScore3) {
			achievementLevel = 3;
		}
		return achievementLevel;
	}
	
	public Object[][] fetchStrengtheningByDate(int idUser, String date) throws SQLException {
		Object[][] strengtheningDatas = new Object[100][3];
		int index = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Strengthening WHERE idUser = "+idUser+" AND date = '"+date+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("idStrengthening");
			String mov = rs.getString("movementType");
			int num = rs.getInt("number");
			strengtheningDatas[index][0] = new Integer(id);
			strengtheningDatas[index][1] = mov;
			strengtheningDatas[index][2] = new Integer(num);
			index++;
		}
		conn.close();
		return strengtheningDatas;
	}
	
	public void deleteStrengtheningData(Object idStrengthening) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("DELETE FROM Strengthening WHERE idStrengthening = "+idStrengthening+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public void updateStrengtheningData(Object idStrengthening, Object number) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE Strengthening SET number = "+number+" WHERE idStrengthening = "+idStrengthening+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	@Override
	public String toString() {
		return "Strengthening [idStrenghening=" + idStrenghening + ", number=" + number + ", typeMovement="
				+ typeMovement + ", date=" + date + "]";
	}
	
	
}
