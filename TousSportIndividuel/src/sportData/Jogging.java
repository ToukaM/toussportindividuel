package sportData;
/**
 * 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import userData.User;

/**
 * @author Touka
 * Jogging class defines the atributs of jogging sport
 */
public class Jogging implements Sport {
	private int idJogging;
	private int distance; //The distance made during the course
	private int duration; //How long the course last
	private int speed; //The speed is calculated automatically using the distance and the duration (speed=distance/duration)
	private String date;//The date of the sport session
	
	public Jogging(int idJogging, int distance, int duration, int speed, String date) {
		super();
		this.idJogging = idJogging;
		this.distance = distance;
		this.duration = duration;
		this.speed = speed;
		this.date = date;
	}
	public Jogging() {
		super();
	}
	public int getIdJogging() {
		return idJogging;
	}
	public void setIdJogging(int idJogging) {
		this.idJogging = idJogging;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getSpeed() {
		speed = this.distance/this.duration;
		return speed;
	}
	/**
	 * Warning! This method should always be called after setDuration(duration) and setDistance(distance)
	 */
	public void setSpeed() {
		this.speed = this.distance/this.duration;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public void registerIntoDB(User user,int distance,int duration,String date) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = distance+","+duration+",'"+date+"',"+user.getIdUser()+")";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO Jogging (distance,duration,date,idUser) VALUES ("+values+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public ArrayList<Jogging> fetchJoggingData(int idUser) throws SQLException {
		ArrayList<Jogging> joggingData = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Jogging WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Jogging jogging = new Jogging();
			jogging.setIdJogging(rs.getInt("idJogging"));
			jogging.setDistance(rs.getInt("distance"));
			jogging.setDuration(rs.getInt("duration"));
			jogging.setDate(rs.getString("date"));
			jogging.setSpeed();
			joggingData.add(jogging);
		}
		conn.close();
		return joggingData;
	}
	
	protected int scoreAchievement(int idUser) throws SQLException {
		int score = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT SUM(duration) FROM Jogging WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		rs.next();
		if(rs.getString(1) != null) {
			score = Integer.parseInt(rs.getString(1));
		}
		conn.close();
		return score;
	}
	public int levelAchievementJo(int idUser) throws SQLException {
		int achievementLevel = 0;
		int score = 0, requierdScore1 = 600, requierdScore2 = 2000, requierdScore3 = 6000;
		score = scoreAchievement(idUser);
		if(score >= requierdScore1 && score < requierdScore2) {
			achievementLevel = 1;
		} 
		else if(score >= requierdScore2 && score < requierdScore3) {
			achievementLevel = 2;
		}
		else if(score >= requierdScore3) {
			achievementLevel = 3;
		}
		return achievementLevel;
	}
	
	public Object[][] fetchJoggingDataByDate(int idUser, String date) throws SQLException {
		Object[][] joggingDatas = new Object[100][3];
		int index = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Jogging WHERE idUser = "+idUser+" AND date = '"+date+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("idJogging");
			int dis = rs.getInt("distance");
			int dur = rs.getInt("duration");
			joggingDatas[index][0] = new Integer(id);
			System.out.println(joggingDatas[index][0]);
			joggingDatas[index][1] = new Integer(dis);
			System.out.println(joggingDatas[index][1]);
			joggingDatas[index][2] = new Integer(dur);
			System.out.println(joggingDatas[index][2]);
			index++;
		}
		conn.close();
		return joggingDatas;
	}
	
	public void deleteJoggingData(Object idJogging) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("DELETE FROM Jogging WHERE idJogging = "+idJogging+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public void updateJoggingData(Object idJogging, Object distance, Object duration) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE Jogging SET duration = "+duration+" WHERE idJogging = "+idJogging+"");
	    ps1.executeUpdate();
	    PreparedStatement ps =(PreparedStatement) conn.prepareStatement("UPDATE Jogging SET distance = "+distance+" WHERE idJogging = "+idJogging+"");
	    ps.executeUpdate();
	    conn.close();
	}
	
	@Override
	public String toString() {
		return "Jogging [idJogging=" + idJogging + ", distance=" + distance + ", duration=" + duration + ", speed="
				+ speed + ", date=" + date + "]";
	}
	
}
