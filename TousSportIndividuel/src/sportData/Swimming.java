package sportData;
/**
 * 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import userData.User;

/**
 * @author Touka
 * Swimming class defines the attributes of swimming sport
 */
public class Swimming implements Sport {
	private int idSwimming; 
	private int distanceSwimmed; //The distance the swimmer swimmed
	private int durationSwim; //How much the long the training last
	private String swimType; //Papillon, Dos, Brasse, Crawl
	private String date; // Date of the session
	public Swimming(int idSwimming, int distanceSwimmed, int durationSwim, String swimType, String date) {
		super();
		this.idSwimming = idSwimming;
		this.distanceSwimmed = distanceSwimmed;
		this.durationSwim = durationSwim;
		this.swimType = swimType;
		this.date = date;
	}
	public Swimming() {
		super();
	}
	public int getIdSwimming() {
		return idSwimming;
	}
	public void setIdSwimming(int idSwimming) {
		this.idSwimming = idSwimming;
	}
	public int getDistanceSwimmed() {
		return distanceSwimmed;
	}
	public void setDistanceSwimmed(int distanceSwimmed) {
		this.distanceSwimmed = distanceSwimmed;
	}
	public int getDurationSwim() {
		return durationSwim;
	}
	public void setDurationSwim(int durationSwim) {
		this.durationSwim = durationSwim;
	}
	public String getSwimType() {
		return swimType;
	}
	public void setSwimType(String swimType) {
		this.swimType = swimType;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void registerIntoDB(User user,int distanceSwimmed,int durationSwim,String swimType, String date) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = distanceSwimmed+","+durationSwim+",'"+swimType+"','"+date+"',"+user.getIdUser()+")";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO Swimming (distanceSwimmed,durationSwim,swimType,date,idUser) VALUES ("+values+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public ArrayList<Swimming> fetchSwimmingData(int idUser) throws SQLException {
		ArrayList<Swimming> swimmingData = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Swimming WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Swimming swimming = new Swimming();
			swimming.setIdSwimming(rs.getInt("idSwimming"));
			swimming.setDistanceSwimmed(rs.getInt("distanceSwimmed"));
			swimming.setDurationSwim(rs.getInt("durationSwim"));
			swimming.setDate(rs.getString("date"));
			swimming.setSwimType(rs.getString("swimType"));
			swimmingData.add(swimming);
		}
		conn.close();
		return swimmingData;
	}
	
	public int getTotalDistanceSwimmed(int idUser, String swimType) throws SQLException {
		int totalDistance = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT distanceSwimmed FROM Swimming WHERE idUser = "+idUser+" AND swimType ='"+swimType+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			totalDistance = totalDistance + rs.getInt("distanceSwimmed");
		}
		conn.close();
		return totalDistance;
	}
	
	protected int scoreAchievement(int idUser) throws SQLException {
		int score = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT SUM(durationSwim) FROM Swimming WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		rs.next();
		if(rs.getString(1) != null) {
			score = Integer.parseInt(rs.getString(1));
		}
		conn.close();
		return score;
	}
	public int levelAchievementSw(int idUser) throws SQLException {
		int achievementLevel = 0;
		int score = 0, requierdScore1 = 600, requierdScore2 = 1800, requierdScore3 = 6000;
		score = scoreAchievement(idUser);
		if(score >= requierdScore1 && score < requierdScore2) {
			achievementLevel = 1;
		} 
		else if(score >= requierdScore2 && score < requierdScore3) {
			achievementLevel = 2;
		}
		else if(score >= requierdScore3) {
			achievementLevel = 3;
		}
		return achievementLevel;
	}
	
	public Object[][] fetchSwimmingByDate(int idUser, String date) throws SQLException {
		Object[][] swimmingDatas = new Object[100][4];
		int index = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Swimming WHERE idUser = "+idUser+" AND date = '"+date+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("idSwimming");
			String mov = rs.getString("swimType");
			int dis = rs.getInt("distanceSwimmed");
			int dur = rs.getInt("durationSwim");
			swimmingDatas[index][0] = new Integer(id);
			swimmingDatas[index][1] = mov;
			swimmingDatas[index][2] = new Integer(dis);
			swimmingDatas[index][3] = new Integer(dur);
			index++;
		}
		conn.close();
		return swimmingDatas;
	}
	
	public void deleteSwimmingData(Object idSwimming) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("DELETE FROM Swimming WHERE idSwimming = "+idSwimming+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public void updateSwimmingData(Object idSwimming, Object distance, Object duration) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE Swimming SET distanceSwimmed = "+distance+" WHERE idSwimming = "+idSwimming+"");
	    ps1.executeUpdate();
	    PreparedStatement ps =(PreparedStatement) conn.prepareStatement("UPDATE Swimming SET durationSwim = "+duration+" WHERE idSwimming = "+idSwimming+"");
	    ps.executeUpdate();
	    conn.close();
	}
	
	@Override
	public String toString() {
		return "Swimming [idSwimming=" + idSwimming + ", distanceSwimmed=" + distanceSwimmed + ", durationSwim="
				+ durationSwim + ", swimType=" + swimType + ", date=" + date + "]";
	}
	
	
}
