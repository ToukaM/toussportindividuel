package sportData;
/**
 * 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import userData.User;

/**
 * @author Touka
 * Bowling class defines the attributes of bowling sport
 */
public class Bowling implements Sport {
	private int idBowling;
	private int onePoint; //How many times the player scored one point
	private int twoPoint; //How many times the player scored two points
	private int threePoint; //How many times the player scored three points
	private int fourPoint; //How many times the player scored four points
	private int fivePoint; //How many times the player scored five points
	private int sixPoint; //How many times the player scored six points
	private int sevenPoint; //How many times the player scored seven points
	private int eightPoint; //How many times the player scored eight points
	private int ninePoint; //How many times the player scored nine points
	private int spare; //How many times the player scored a spare
	private int strike; //How many times the player scored a strike
	private int openFrame; //How many times the player scored an open frame
	private int split; //How many times the player scored a split
	private int result; //The total of the game
	private String date; // Date of the session
	public Bowling(int idBowling, int onePoint, int twoPoint, int threePoint, int fourPoint, int fivePoint,
			int sixPoint, int sevenPoint, int eightPoint, int ninePoint, int spare, int strike, int openFrame,
			int split, int result, String date) {
		super();
		this.idBowling = idBowling;
		this.onePoint = onePoint;
		this.twoPoint = twoPoint;
		this.threePoint = threePoint;
		this.fourPoint = fourPoint;
		this.fivePoint = fivePoint;
		this.sixPoint = sixPoint;
		this.sevenPoint = sevenPoint;
		this.eightPoint = eightPoint;
		this.ninePoint = ninePoint;
		this.spare = spare;
		this.strike = strike;
		this.openFrame = openFrame;
		this.split = split;
		this.result = result;
		this.date = date;
	}
	public Bowling() {
		super();
	}
	public int getIdBowling() {
		return idBowling;
	}
	public void setIdBowling(int idBowling) {
		this.idBowling = idBowling;
	}
	public int getOnePoint() {
		return onePoint;
	}
	public void setOnePoint(int onePoint) {
		this.onePoint = onePoint;
	}
	public int getTwoPoint() {
		return twoPoint;
	}
	public void setTwoPoint(int twoPoint) {
		this.twoPoint = twoPoint;
	}
	public int getThreePoint() {
		return threePoint;
	}
	public void setThreePoint(int threePoint) {
		this.threePoint = threePoint;
	}
	public int getFourPoint() {
		return fourPoint;
	}
	public void setFourPoint(int fourPoint) {
		this.fourPoint = fourPoint;
	}
	public int getFivePoint() {
		return fivePoint;
	}
	public void setFivePoint(int fivePoint) {
		this.fivePoint = fivePoint;
	}
	public int getSixPoint() {
		return sixPoint;
	}
	public void setSixPoint(int sixPoint) {
		this.sixPoint = sixPoint;
	}
	public int getSevenPoint() {
		return sevenPoint;
	}
	public void setSevenPoint(int sevenPoint) {
		this.sevenPoint = sevenPoint;
	}
	public int getEightPoint() {
		return eightPoint;
	}
	public void setEightPoint(int eightPoint) {
		this.eightPoint = eightPoint;
	}
	public int getNinePoint() {
		return ninePoint;
	}
	public void setNinePoint(int ninePoint) {
		this.ninePoint = ninePoint;
	}
	public int getSpare() {
		return spare;
	}
	public void setSpare(int spare) {
		this.spare = spare;
	}
	public int getStrike() {
		return strike;
	}
	public void setStrike(int strike) {
		this.strike = strike;
	}
	public int getOpenFrame() {
		return openFrame;
	}
	public void setOpenFrame(int openFrame) {
		this.openFrame = openFrame;
	}
	public int getSplit() {
		return split;
	}
	public void setSplit(int split) {
		this.split = split;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void registerIntoDB(User user,int one,int two,int three,int four,int five,int six,int seven,int eight,int nine,int spare, int strike,int openFrame, int split,int result, String date) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = one+","+two+","+three+","+four+","+five+","+six+","+seven+","+eight+","+nine+","+spare+","+strike+","+openFrame+","+split+","+result+",'"+date+"',"+user.getIdUser()+")";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO Bowling (onePoint,twoPoint,threePoint,fourPoint,fivePoint,sixPoint,sevenPoint,eightPoint,ninePoint,spare,strike,openFrame,split,result,date,idUser) VALUES ("+values+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public ArrayList<Bowling> fetchBowlingData(int idUser) throws SQLException {
		ArrayList<Bowling> bowlingData = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Bowling WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Bowling bowling = new Bowling();
			bowling.setIdBowling(rs.getInt("idBowling"));
			bowling.setOnePoint(rs.getInt("onePoint"));
			bowling.setTwoPoint(rs.getInt("twoPoint"));
			bowling.setThreePoint(rs.getInt("threePoint"));
			bowling.setFourPoint(rs.getInt("fourPoint"));
			bowling.setFivePoint(rs.getInt("fivePoint"));
			bowling.setSixPoint(rs.getInt("sixPoint"));
			bowling.setSevenPoint(rs.getInt("sevenPoint"));
			bowling.setEightPoint(rs.getInt("eightPoint"));
			bowling.setNinePoint(rs.getInt("ninePoint"));
			bowling.setSpare(rs.getInt("spare"));
			bowling.setStrike(rs.getInt("strike"));
			bowling.setSplit(rs.getInt("split"));
			bowling.setOpenFrame(rs.getInt("openFrame"));
			bowling.setResult(rs.getInt("result"));
			bowling.setDate(rs.getString("date"));
			bowlingData.add(bowling);
		}
		conn.close();
		return bowlingData;
	}
	public int totalSpares(int idUser) throws SQLException {
		int total = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT spare FROM Bowling WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			total = total + rs.getInt("spare");
		}
		conn.close();
		return total;
	}
	public int totalSplits(int idUser) throws SQLException {
		int total = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT split FROM Bowling WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			total = total + rs.getInt("split");
		}
		conn.close();
		return total;
	}
	public int totalOpenFrames(int idUser) throws SQLException {
		int total = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT openFrame FROM Bowling WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			total = total + rs.getInt("openFrame");
		}
		conn.close();
		return total;
	}
	public int totalStrikes(int idUser) throws SQLException {
		int total = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT strike FROM Bowling WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			total = total + rs.getInt("strike");
		}
		conn.close();
		return total;
	}
	
	protected int scoreAchievement(int idUser) throws SQLException {
		int score = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT SUM(result) FROM Bowling WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		rs.next();
		if(rs.getString(1)!=null) {
			score = Integer.parseInt(rs.getString(1));
		}
		conn.close();
		return score;
	}
	public int levelAchievementBo(int idUser) throws SQLException {
		int achievementLevel = 0;
		int score = 0, requierdScore1 = 500, requierdScore2 = 1500, requierdScore3 = 4000;
		score = scoreAchievement(idUser);
		if(score >= requierdScore1 && score < requierdScore2) {
			achievementLevel = 1;
		}
		else if(score >= requierdScore2 && score < requierdScore3) {
			achievementLevel = 2;
		}
		else if(score >= requierdScore3) {
			achievementLevel = 3;
		}
		return achievementLevel;
	}
	
	public Object[][] fetchBowlingByDate(int idUser, String date) throws SQLException {
		Object[][] bowlingDatas = new Object[100][6];
		int index = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM Bowling WHERE idUser = "+idUser+" AND date = '"+date+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("idBowling");
			int spa = rs.getInt("spare");
			int str = rs.getInt("strike");
			int spl = rs.getInt("split");
			int ope = rs.getInt("openFrame");
			int res = rs.getInt("result");
			bowlingDatas[index][0] = new Integer(id);
			bowlingDatas[index][1] = new Integer(spa);
			bowlingDatas[index][2] = new Integer(str);
			bowlingDatas[index][3] = new Integer(spl);
			bowlingDatas[index][4] = new Integer(ope);
			bowlingDatas[index][5] = new Integer(res);
			index++;
		}
		conn.close();
		return bowlingDatas;
	}
	
	public void deleteBowlingData(Object idBowling) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("DELETE FROM Bowling WHERE idBowling = "+idBowling+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public void updateBowlingData(Object idBowling, Object spare, Object strike, Object split, Object openFrame, Object result) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE Bowling SET spare = "+spare+", strike = "+strike+", split = "+split+", openFrame = "+openFrame+", result = "+result+" WHERE idBowling = "+idBowling+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	@Override
	public String toString() {
		return "Bowling [idBowling=" + idBowling + ", onePoint=" + onePoint + ", twoPoint=" + twoPoint + ", threePoint="
				+ threePoint + ", fourPoint=" + fourPoint + ", fivePoint=" + fivePoint + ", sixPoint=" + sixPoint
				+ ", sevenPoint=" + sevenPoint + ", eightPoint=" + eightPoint + ", ninePoint=" + ninePoint + ", spare="
				+ spare + ", strike=" + strike + ", openFrame=" + openFrame + ", split=" + split + ", result=" + result
				+ ", date=" + date + "]";
	}
	
}
