package sportData;
/**
 * 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import userData.User;

/**
 * @author Touka
 * ShotPut class defines the attributes of shot put sport
 */
public class ShotPut implements Sport {
	private int idShotPut; 
	private int distance; //The distance the ball ran through before touching the floor
	private int attemps; //Number of attempts in on game
	private String date; // Date of the session
	public ShotPut(int idShotPut, int distance, int attemps, String date) {
		super();
		this.idShotPut = idShotPut;
		this.distance = distance;
		this.attemps = attemps;
		this.date = date;
	}
	public ShotPut() {
		super();
	}
	public int getIdShotPut() {
		return idShotPut;
	}
	public void setIdShotPut(int idShotPut) {
		this.idShotPut = idShotPut;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getAttemps() {
		return attemps;
	}
	public void setAttemps(int attemps) {
		this.attemps = attemps;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void registerIntoDB(User user,int distance,int attempts, String date) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = distance+","+attempts+",'"+date+"',"+user.getIdUser()+")";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO ShotPut (distanceShot,attemptsShot,date,idUser) VALUES ("+values+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public ArrayList<ShotPut> fetchShotPutData(int idUser) throws SQLException {
		ArrayList<ShotPut> shotPutData = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM ShotPut WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			ShotPut shotPut = new ShotPut();
			shotPut.setIdShotPut(rs.getInt("idShotPut"));
			shotPut.setDistance(rs.getInt("distanceShot"));
			shotPut.setAttemps(rs.getInt("attemptsShot"));
			shotPut.setDate(rs.getString("date"));
			shotPutData.add(shotPut);
		}
		conn.close();
		return shotPutData;	
	}
	
	protected int scoreAchievement(int idUser) throws SQLException {
		int score = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT SUM(attemptsShot) FROM ShotPut WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		rs.next();
		if(rs.getString(1) != null) {
			score = Integer.parseInt(rs.getString(1));
		}
		conn.close();
		return score;
	}
	public int levelAchievementSo(int idUser) throws SQLException {
		int achievementLevel = 0;
		int score = 0, requierdScore1 = 200, requierdScore2 = 400, requierdScore3 = 1000;
		score = scoreAchievement(idUser);
		if(score >= requierdScore1 && score < requierdScore2) {
			achievementLevel = 1;
		} 
		else if(score >= requierdScore2 && score < requierdScore3) {
			achievementLevel = 2;
		}
		else if(score >= requierdScore3) {
			achievementLevel = 3;
		}
		return achievementLevel;
	}
	
	public Object[][] fetchShotPutByDate(int idUser, String date) throws SQLException {
		Object[][] shotPutDatas = new Object[100][3];
		int index = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM ShotPut WHERE idUser = "+idUser+" AND date = '"+date+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("idShotPut");
			int dis = rs.getInt("distanceShot");
			int att = rs.getInt("attemptsShot");
			shotPutDatas[index][0] = new Integer(id);
			shotPutDatas[index][1] = new Integer(dis);
			shotPutDatas[index][2] = new Integer(att);
			index++;
		}
		conn.close();
		return shotPutDatas;
	}
	
	public void deleteShotPutData(Object idShotPut) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("DELETE FROM ShotPut WHERE idShotPut = "+idShotPut+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public void updateShotPutData(Object idShotPut, Object distance, Object attempts) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE ShotPut SET attemptsShot = "+attempts+" WHERE idShotPut = "+idShotPut+"");
	    ps1.executeUpdate();
	    PreparedStatement ps =(PreparedStatement) conn.prepareStatement("UPDATE ShotPut SET distanceShot = "+distance+" WHERE idShotPut = "+idShotPut+"");
	    ps.executeUpdate();
	    conn.close();
	}
	
	@Override
	public String toString() {
		return "ShotPut [idShotPut=" + idShotPut + ", distance=" + distance + ", attemps=" + attemps + ", date="
				+ date + "]";
	}
	
	
}
