package sportData;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;

/**
 * 
 */

/**
 * @author Touka
 * Sport Interface is a class which groups all the sports Used in the app
 */
public interface Sport {
	
	public default ArrayList<String> fetchDates(int idUser, String selectedSport) throws SQLException{
		if(selectedSport==null) {
			return null;
		}
		else {
			ArrayList<String> dates = new ArrayList<>();
			Connection conn = DBConnection.connectToDB();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT DISTINCT date FROM "+selectedSport+" WHERE idUser = "+idUser+"");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				dates.add(rs.getString("date"));
			}
			conn.close();
			return dates;
		}
	}
	
}
