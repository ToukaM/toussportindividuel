/**
 * 
 */
package sportData;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;
import userData.User;

/**
 * @author Touka
 * LongJump is a class which defines the attributes of the long jump sport
 */
public class LongJump implements Sport {
	private int idLongJump;
	private String date; // Date of the session
	private int distanceJumped;
	private int attemptsJump;
	public LongJump(int idLongJump, int distanceJumped, int attemptsJump, String date) {
		super();
		this.idLongJump = idLongJump;
		this.distanceJumped = distanceJumped;
		this.attemptsJump = attemptsJump;
		this.date = date;
	}
	public LongJump() {
		super();
	}
	public int getIdLongJump() {
		return idLongJump;
	}
	public void setIdLongJump(int idLongJump) {
		this.idLongJump = idLongJump;
	}
	public int getDistanceJumped() {
		return distanceJumped;
	}
	public void setDistanceJumped(int distanceJumped) {
		this.distanceJumped = distanceJumped;
	}
	public int getAttemptsJump() {
		return attemptsJump;
	}
	public void setAttemptsJump(int attemptsJump) {
		this.attemptsJump = attemptsJump;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void registerIntoDB(User user,int distanceJumped,int attemptsJump, String date) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		String values = distanceJumped+","+attemptsJump+",'"+date+"',"+user.getIdUser()+")";
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO LongJump (distanceJumped,attemptsJump,date,idUser) VALUES ("+values+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	public ArrayList<LongJump> fetchLongJumpData(int idUser) throws SQLException {
		ArrayList<LongJump> longJumpData = new ArrayList<>();
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM LongJump WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			LongJump longJump = new LongJump();
			longJump.setIdLongJump(rs.getInt("idLongJump"));
			longJump.setDistanceJumped(rs.getInt("distanceJumped"));
			longJump.setAttemptsJump(rs.getInt("attemptsJump"));
			longJump.setDate(rs.getString("date"));
			longJumpData.add(longJump);
		}
		conn.close();
		return longJumpData;
	}
	
	protected int scoreAchievement(int idUser) throws SQLException {
		int score = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT SUM(attemptsJump) FROM LongJump WHERE idUser = "+idUser+"");
		ResultSet rs = ps.executeQuery();
		rs.next();
		if(rs.getString(1) != null) {
			score = Integer.parseInt(rs.getString(1));
		}
		conn.close();
		return score;
	}
	public int levelAchievementLo(int idUser) throws SQLException {
		int achievementLevel = 0;
		int score = 0, requierdScore1 = 200, requierdScore2 = 600, requierdScore3 = 1500;
		score = scoreAchievement(idUser);
		if(score >= requierdScore1 && score < requierdScore2) {
			achievementLevel = 1;
		} 
		else if(score >= requierdScore2 && score < requierdScore3) {
			achievementLevel = 2;
		}
		else if(score >= requierdScore3) {
			achievementLevel = 3;
		}
		return achievementLevel;
	}
	
	public Object[][] fetchLongJumpDataByDate(int idUser, String date) throws SQLException {
		Object[][] longJumpDatas = new Object[100][3];
		int index = 0;
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement("SELECT * FROM LongJump WHERE idUser = "+idUser+" AND date = '"+date+"'");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("idLongJump");
			int dis = rs.getInt("distanceJumped");
			int att = rs.getInt("attemptsJump");
			longJumpDatas[index][0] = new Integer(id);
			longJumpDatas[index][1] = new Integer(dis);
			longJumpDatas[index][2] = new Integer(att);
			index++;
		}
		conn.close();
		return longJumpDatas;
	}
	
	public void deleteLongJumpData(Object idLongJump) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("DELETE FROM LongJump WHERE idLongJump = "+idLongJump+"");
	    ps1.executeUpdate();
	    conn.close();
	}
	
	public void updateLongJumpData(Object idLongJump, Object distance, Object attempts) throws SQLException {
		Connection conn = DBConnection.connectToDB();
		PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("UPDATE LongJump SET attemptsJump = "+attempts+" WHERE idLongJump = "+idLongJump+"");
	    ps1.executeUpdate();
	    PreparedStatement ps =(PreparedStatement) conn.prepareStatement("UPDATE LongJump SET distanceJumped = "+distance+" WHERE idLongJump = "+idLongJump+"");
	    ps.executeUpdate();
	    conn.close();
	}
	
	
	@Override
	public String toString() {
		return "LongJump [idLongJump=" + idLongJump + ", date=" + date + ", distanceJumped=" + distanceJumped
				+ ", attemptsJump=" + attemptsJump + "]";
	}
	
	
}
