package ihm;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import userData.User;
import javax.swing.border.LineBorder;

import ihm.formSport.*;
import ihm.dataDisplayer.*;

public class MySports extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton userWallButton;
	private JButton mySportsButton;
	private JButton sportStatisticsButton;
	private JButton searchButton;
	private JButton logoutButton;
	private ArrayList<String> sports;
	private User user;
	private static String sportClicked = null;

	/**
	 * Launch the application.
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com", "azerty");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MySports frame = new MySports(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public MySports(User user) {
		this.user = user;
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		setResizable(false);
		
		Font fontButton = new Font("Microsoft YaHei UI Light", Font.BOLD, 18);
		
		/*--------Sports list---------*/
		
		
		JLabel lblMySports = new JLabel("My Sports :");
		lblMySports.setForeground(Color.WHITE);
		lblMySports.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		lblMySports.setBounds(67, 52, 195, 32);
		contentPane.add(lblMySports);
		
		
		JScrollPane scrollPaneSports = new JScrollPane();
		scrollPaneSports.setBounds(67, 120, 195, 247);
		contentPane.add(scrollPaneSports);
		
		sports = user.getSports();
		JList listSports = new JList(sports.toArray()); 
		listSports.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		listSports.setFixedCellHeight(40);
		scrollPaneSports.setViewportView(listSports);
		
		listSports.setFont(listSports.getFont().deriveFont(18f));

		
		JButton btnAddSport = new JButton("Add Sport");
		btnAddSport.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAddSport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddSport addSport = new AddSport(user);
				addSport.setVisible(true);
				dispose();
			}
		});
		btnAddSport.setBounds(92, 375, 139, 32);
		btnAddSport.setBackground(Color.LIGHT_GRAY);
		contentPane.add(btnAddSport);
		
		if( user.getSports().size()==6)
			btnAddSport.setVisible(false);
		
		/*-------------navigation buttons-----------------*/
		
		userWallButton = new JButton("My Wall");
		userWallButton.setFont(fontButton);
		userWallButton.setBackground(Color.LIGHT_GRAY);
		userWallButton.setBounds(20, 500, 150, 45);
		contentPane.add(userWallButton);
		userWallButton.addActionListener(this);
		
		mySportsButton = new JButton("My Sports");
		mySportsButton.setFont(fontButton);
		mySportsButton.setBackground(Color.LIGHT_GRAY);
		mySportsButton.setBounds(190, 500, 150, 45);
		contentPane.add(mySportsButton);
		mySportsButton.setEnabled(false);
		
		sportStatisticsButton = new JButton("Sport Stats");
		sportStatisticsButton.setFont(fontButton);
		sportStatisticsButton.setBackground(Color.LIGHT_GRAY);
		sportStatisticsButton.setBounds(360, 500, 150, 45);
		contentPane.add(sportStatisticsButton);
		sportStatisticsButton.addActionListener(this);
		
		searchButton = new JButton("Search");
		searchButton.setFont(fontButton);
		searchButton.setBackground(Color.LIGHT_GRAY);
		searchButton.setBounds(530, 500, 150, 45);
		contentPane.add(searchButton);
		searchButton.addActionListener(this);
		
		logoutButton = new JButton("Logout");
		logoutButton.setFont(fontButton);
		logoutButton.setBackground(Color.LIGHT_GRAY);
		logoutButton.setBounds(817, 500, 116, 45);
		contentPane.add(logoutButton);
		
		JButton addDataButton = new JButton("Add data");
		addDataButton.setFont(fontButton);
		addDataButton.setBackground(Color.LIGHT_GRAY);
		addDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (sportClicked != null) {
				switch (sportClicked){
				 	case "Bowling":
				 		FormBowling formBowling = new FormBowling(user);
						formBowling.setVisible(true);
						dispose();
				 		break;	
				 	case "Jogging":
				 		FormJogging formJogging = new FormJogging(user);
				 		formJogging.setVisible(true);
				 		dispose();
				 		break;	
				 	case "LongJump":  
				 		FormLongJump formLongJump = new FormLongJump(user);
				 		formLongJump.setVisible(true);
				 		dispose();
				 		break;
				 	case "ShotPut":  
				 		FormShotPut formShotPut = new FormShotPut(user);
				 		formShotPut.setVisible(true);
				 		dispose();
				 		break;
				 	case "Strengthening":  
				 		FormStrengthening formStrengthening = new FormStrengthening(user);
				 		formStrengthening.setVisible(true);
				 		dispose();
				 		break;
				 	case "Swimming":  
				 		FormSwimming formSwimming = new FormSwimming(user);
				 		formSwimming.setVisible(true);
				 		dispose();
				 		break;
					} 
				}
				
			}
		});
		addDataButton.setBounds(348, 185, 195, 45);
		contentPane.add(addDataButton);
	
		sports = user.getSports();
		
		JButton displayDataButton = new JButton("History");
		displayDataButton.setBounds(683, 185, 195, 45);
		displayDataButton.setFont(fontButton);
		displayDataButton.setBackground(Color.LIGHT_GRAY);
		displayDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (sportClicked){
			 	case "Bowling":
			 		BowlingDataDisplayer bowlingDataDisplayer = new BowlingDataDisplayer(user,sportClicked);
			 		bowlingDataDisplayer.setVisible(true);
					dispose();
			 		break;	
			 	case "Jogging":
			 		JoggingDataDisplayer joggingDataDisplayer = new JoggingDataDisplayer(user,sportClicked);
			 		joggingDataDisplayer.setVisible(true);
			 		dispose();
			 		break;	
			 	case "LongJump":  
			 		LongJumpDataDisplayer longJumpDataDisplayer = new LongJumpDataDisplayer(user,sportClicked);
			 		longJumpDataDisplayer.setVisible(true);
			 		dispose();
			 		break;
			 	case "ShotPut":  
			 		ShotPutDataDisplayer shotPutDataDisplayer = new ShotPutDataDisplayer(user,sportClicked);
			 		shotPutDataDisplayer.setVisible(true);
			 		dispose();
			 		break;
			 	case "Strengthening":  
			 		StrengtheningDataDisplayer strengtheningDataDisplayer = new StrengtheningDataDisplayer(user,sportClicked);
			 		strengtheningDataDisplayer.setVisible(true);
			 		dispose();
			 		break;
			 	case "Swimming":  
			 		SwimmingDataDisplayer swimmingDataDisplayer = new SwimmingDataDisplayer(user,sportClicked);
			 		swimmingDataDisplayer.setVisible(true);
			 		dispose();
			 		break;
				} 
			}
		});
		contentPane.add(displayDataButton);
		
		
		logoutButton.addActionListener(this);
		
	
		listSports.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				if (me.getClickCount() == 1) {
					int index = listSports.locationToIndex(me.getPoint());	
					sportClicked = sports.get(index).toString();
					System.out.println(sportClicked);
				}
			}
		});
	}
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==userWallButton) {
			UserWall userWall = new UserWall(user);
			userWall.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==sportStatisticsButton) {
			SportStatistics sportStatistics;
			try {
				sportStatistics = new SportStatistics(user);
				sportStatistics.setVisible(true);
				this.dispose();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		if(arg0.getSource()==searchButton) {
			Search search = new Search(user);
			search.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==logoutButton) {
			Login login = new Login();
			login.setVisible(true);
			this.dispose();
		}
	}
}
