package ihm;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import sportData.*;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AchievementPanel extends JPanel {

	
	private Bowling bowling = new Bowling();
	private Jogging jogging = new Jogging();
	private LongJump longJump = new LongJump();
	private ShotPut shotPut = new ShotPut();
	private Strengthening strengthening = new Strengthening();
	private Swimming swimming = new Swimming();
	
	/**
	 * Create the panel.
	 */
	public AchievementPanel(int idUser) throws SQLException {
		setBackground(Color.DARK_GRAY);
		setSize(441, 413);
		setLayout(null);

		JLabel lblAchievement = new JLabel("Achievements :");
		lblAchievement.setHorizontalAlignment(SwingConstants.CENTER);
		lblAchievement.setBounds(10, 0, 421, 32);
		lblAchievement.setForeground(Color.WHITE);
		lblAchievement.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		add(lblAchievement);
		
		ImageIcon bowlingIcon = null;
		ImageIcon joggingIcon = null;
		ImageIcon longJumpIcon = null;
		ImageIcon shotPutIcon = null;
		ImageIcon strengtheningIcon = null;
		ImageIcon swimmingIcon = null;
		
		ImageIcon bowling1 = new ImageIcon( "./images/Achievement_Bowling1.png");
		ImageIcon bowling2 = new ImageIcon( "./images/Achievement_Bowling2.png");
		ImageIcon bowling3 = new ImageIcon( "./images/Achievement_Bowling3.png");
		ImageIcon jogging1 = new ImageIcon( "./images/Achievement_Jogging1.png");
		ImageIcon jogging2 = new ImageIcon( "./images/Achievement_Jogging2.png");
		ImageIcon jogging3 = new ImageIcon( "./images/Achievement_Jogging3.png");
		ImageIcon longJump1 = new ImageIcon( "./images/Achievement_LongJump1.png");
		ImageIcon longJump2 = new ImageIcon( "./images/Achievement_LongJump2.png");
		ImageIcon longJump3 = new ImageIcon( "./images/Achievement_LongJump3.png");
		ImageIcon shotPut1 = new ImageIcon( "./images/Achievement_ShotPut1.png");
		ImageIcon shotPut2 = new ImageIcon( "./images/Achievement_ShotPut2.png");
		ImageIcon shotPut3 = new ImageIcon( "./images/Achievement_ShotPut3.png");
		ImageIcon strengthening1 = new ImageIcon( "./images/Achievement_Strengthening1.png");
		ImageIcon strengthening2 = new ImageIcon( "./images/Achievement_Strengthening2.png");
		ImageIcon strengthening3 = new ImageIcon( "./images/Achievement_Strengthening3.png");
		ImageIcon swimming1 = new ImageIcon( "./images/Achievement_Swimming1.png");
		ImageIcon swimming2 = new ImageIcon( "./images/Achievement_Swimming2.png");
		ImageIcon swimming3 = new ImageIcon( "./images/Achievement_Swimming3.png");
		
		ArrayList<JLabel> listLabelAchievement = new ArrayList<JLabel>();
		
		JLabel lbl_img_1_1 = new JLabel();
		lbl_img_1_1.setForeground(Color.WHITE);
		lbl_img_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_img_1_1.setBounds(0, 81, 144, 144);
		add(lbl_img_1_1);
		listLabelAchievement.add(lbl_img_1_1);
		
		JLabel lbl_img_1_2 = new JLabel();
		lbl_img_1_2.setForeground(Color.WHITE);
		lbl_img_1_2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_img_1_2.setBounds(148, 81, 144, 144);
		add(lbl_img_1_2);
		listLabelAchievement.add(lbl_img_1_2);
		
		JLabel lbl_img_1_3 = new JLabel();
		lbl_img_1_3.setForeground(Color.WHITE);
		lbl_img_1_3.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_img_1_3.setBounds(296, 81, 144, 144);
		add(lbl_img_1_3);
		listLabelAchievement.add(lbl_img_1_3);
		
		JLabel lbl_img_2_1 = new JLabel();
		lbl_img_2_1.setForeground(Color.WHITE);
		lbl_img_2_1.setBounds(0, 268, 144, 144);
		lbl_img_2_1.setHorizontalAlignment(SwingConstants.CENTER);
		add(lbl_img_2_1);
		listLabelAchievement.add(lbl_img_2_1);
		
		JLabel lbl_img_2_2 = new JLabel();
		lbl_img_2_2.setForeground(Color.WHITE);
		lbl_img_2_2.setBounds(148, 268, 144, 144);
		lbl_img_2_2.setHorizontalAlignment(SwingConstants.CENTER);
		add(lbl_img_2_2);
		listLabelAchievement.add(lbl_img_2_2);
		
		JLabel lbl_img_2_3 = new JLabel();
		lbl_img_2_3.setForeground(Color.WHITE);
		lbl_img_2_3.setBounds(296, 268, 144, 144);
		lbl_img_2_3.setHorizontalAlignment(SwingConstants.CENTER);
		add(lbl_img_2_3);
		listLabelAchievement.add(lbl_img_2_3);
		
		JButton btnClicHereTo = new JButton("Click to see");
		btnClicHereTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame allAchievementsFrame = new JFrame();
				allAchievementsFrame.setSize(550,550);
				allAchievementsFrame.setTitle("Rules");
				allAchievementsFrame.setLocationRelativeTo(null);
				ImageIcon icon = new ImageIcon("./images/achievement_info.jpg");
				JLabel label = new JLabel(icon);
				allAchievementsFrame.add(label);
				allAchievementsFrame.setVisible(true);
				allAchievementsFrame.setBackground(Color.WHITE);
			}
		});
		btnClicHereTo.setBackground(Color.GRAY);
		btnClicHereTo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnClicHereTo.setBounds(342, 30, 89, 23);
		add(btnClicHereTo);
		
		int levelBowling = bowling.levelAchievementBo(idUser);
		int levelJogging = jogging.levelAchievementJo(idUser);
		int levelLongJump = longJump.levelAchievementLo(idUser);
		int levelShotPut = shotPut.levelAchievementSo(idUser);
		int levelStrengthening = strengthening.levelAchievementSt(idUser);
		int levelSwimming = swimming.levelAchievementSw(idUser);
		
		ArrayList<ImageIcon> imgAchievements = new ArrayList<ImageIcon>();
		
		switch (levelBowling){
	 		case 1: bowlingIcon = bowling1;
	 			break;
	 		case 2: bowlingIcon = bowling2;
	 			break;
	 		case 3: bowlingIcon = bowling3;
	 			break;
		}
		if(bowlingIcon != null) {
			imgAchievements.add(bowlingIcon);
			
		}
		switch (levelJogging){
	 		case 1: joggingIcon = jogging1;
	 			break;
	 		case 2: joggingIcon = jogging2;
	 			break;
	 		case 3: joggingIcon = jogging3;
	 			break;
		}
		if(joggingIcon != null) {
			imgAchievements.add(joggingIcon);
		}
		switch (levelLongJump){
	 		case 1: longJumpIcon = longJump1;
	 			break;
	 		case 2: longJumpIcon = longJump2;
	 			break;
	 		case 3: longJumpIcon = longJump3;
	 			break;
		}
		if(longJumpIcon != null) {
			imgAchievements.add(longJumpIcon);
		}
		switch (levelShotPut){
	 		case 1: shotPutIcon = shotPut1;
	 			break;
	 		case 2: shotPutIcon = shotPut2;
	 			break;
	 		case 3: shotPutIcon = shotPut3;
	 			break;
		}
		if(shotPutIcon != null) {
			imgAchievements.add(shotPutIcon);
		}
		switch (levelStrengthening){
	 		case 1: strengtheningIcon = strengthening1;
	 			break;
	 		case 2: strengtheningIcon = strengthening2;
	 			break;
	 		case 3: strengtheningIcon = strengthening3;
	 			break;
		}
		if(strengtheningIcon != null) {
			imgAchievements.add(strengtheningIcon);
		}
		switch (levelSwimming){
	 		case 1: swimmingIcon = swimming1;
	 			break;
	 		case 2: swimmingIcon = swimming2;
	 			break;
	 		case 3: swimmingIcon = swimming3;
	 			break;
		}
		if(swimmingIcon != null) {
			imgAchievements.add(swimmingIcon);
		}
		
		/**
		 * Set image in label in order
		 */
		int index = 0;
		for (JLabel lbl : listLabelAchievement) {
			if(index < imgAchievements.size()) {
			 lbl.setIcon(imgAchievements.get(index));
			 index ++;
			}
			
			else break;
		}
		
	}
}
