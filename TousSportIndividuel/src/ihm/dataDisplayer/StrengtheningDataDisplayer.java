package ihm.dataDisplayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ihm.MySports;
import sportData.Strengthening;
import userData.User;
import javax.swing.SwingConstants;

public class StrengtheningDataDisplayer extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private User user;
	private ArrayList<String> dates;
	private Strengthening strengthening = new Strengthening();
	private static Object[][] data = new Object[100][3];
	private JButton deleteButton = new JButton("Delete");
	private static String selectedDate = null;
	private JLabel title = new JLabel("Strengthening Data Displayer");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com", "azerty");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StrengtheningDataDisplayer frame = new StrengtheningDataDisplayer(user,"Strengthening");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StrengtheningDataDisplayer(User user, String selectedSport) {
this.user = user;
		
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null); 
		setContentPane(contentPane);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		
		title.setForeground(Color.WHITE);
		title.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 28));
		title.setBounds(10, 16, 934, 45);
		contentPane.add(title);
		
		JScrollPane scrollPaneSports = new JScrollPane();
		scrollPaneSports.setBounds(67, 120, 195, 247);
		contentPane.add(scrollPaneSports);
		
		try {
			dates = strengthening.fetchDates(user.getIdUser(), selectedSport);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		
		JList listDates = new JList(dates.toArray()); 
		listDates.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		listDates.setFixedCellHeight(40);
		scrollPaneSports.setViewportView(listDates);
		listDates.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		
		deleteButton.setBounds(670, 420, 115, 29);
		contentPane.add(deleteButton);
		deleteButton.setBackground(Color.LIGHT_GRAY);
		
		
		String[] columnNames = {"ID", "Movement", "Number"};
		JTable table = new JTable(data,columnNames);
		table.setRowHeight(25);
		table.setCellSelectionEnabled(true);
		table.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		table.getTableHeader().setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 22));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(374, 120, 472, 247);
		contentPane.add(scrollPane);
		
		JButton updateButton = new JButton("Update");
		updateButton.setBounds(431, 420, 115, 29);
		contentPane.add(updateButton);
		updateButton.setBackground(Color.LIGHT_GRAY);
		updateButton.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Object idStrengthening = table.getValueAt(row, 0);
				//Object movementType = table.getValueAt(row, 1);
				Object number = table.getValueAt(row, 2);
				try {
					strengthening.updateStrengtheningData(idStrengthening, number);
					data = strengthening.fetchStrengtheningByDate(user.getIdUser(), selectedDate);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				int len = data.length;
				for(int i=0;i<len;i++) {
					for(int j=0;j<3;j++) {
						table.setValueAt(data[i][j], i, j);
					}
				}
			}
		});
		
		deleteButton.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Object idStrengthening = table.getValueAt(row, 0);
				try {
					strengthening.deleteStrengtheningData(idStrengthening);
					data = strengthening.fetchStrengtheningByDate(user.getIdUser(), selectedDate);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				int len = data.length;
				for(int i=0;i<len;i++) {
					for(int j=0;j<3;j++) {
						table.setValueAt(data[i][j], i, j);
					}
				}
			}
		});
		
		listDates.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				if (me.getClickCount() == 2) {
					selectedDate = (String) listDates.getSelectedValue();
					try {
						data = strengthening.fetchStrengtheningByDate(user.getIdUser(), selectedDate);
						int len = data.length;
						for(int i=0;i<len;i++) {
							for(int j=0;j<3;j++) {
								table.setValueAt(data[i][j], i, j);
							}
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
			}
		});
		
		JButton btnCancel = new JButton("Go back to my sports");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MySports mySports = new MySports(user);
				mySports.setVisible(true);
				dispose();
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		btnCancel.setBounds(20, 505, 231, 45);
		contentPane.add(btnCancel);
		
		JLabel lblChooseADate = new JLabel("Choose a date :");
		lblChooseADate.setForeground(Color.WHITE);
		lblChooseADate.setBounds(65, 78, 186, 20);
		lblChooseADate.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		contentPane.add(lblChooseADate);
		
	}

}
