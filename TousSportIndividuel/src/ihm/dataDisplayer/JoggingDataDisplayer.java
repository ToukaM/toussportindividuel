package ihm.dataDisplayer;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ihm.MySports;
import sportData.Jogging;
import userData.User;
import javax.swing.JTable;
import java.awt.Font;

public class JoggingDataDisplayer extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private User user;
	private ArrayList<String> dates;
	private Jogging jogging = new Jogging();
	private static Object[][] joggingData = new Object[100][3];
	private JButton deleteButton = new JButton("Delete");
	private static String selectedDate = null;
	private JLabel title = new JLabel("Jogging Data Displayer");
	

	/**
	 * Launch the application.
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com", "azerty");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JoggingDataDisplayer frame = new JoggingDataDisplayer(user,"Jogging");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JoggingDataDisplayer(User user, String selectedSport) {
		this.user = user;
		
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null); 
		setContentPane(contentPane);
		
		title.setForeground(Color.WHITE);
		title.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 28));
		title.setBounds(301, 16, 362, 45);
		contentPane.add(title);
		
		JScrollPane scrollPaneSports = new JScrollPane();
		scrollPaneSports.setBounds(67, 120, 195, 247);
		contentPane.add(scrollPaneSports);
		
		try {
			dates = jogging.fetchDates(user.getIdUser(), selectedSport);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		
		JList listDates = new JList(dates.toArray()); 
		listDates.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		listDates.setFixedCellHeight(40);
		scrollPaneSports.setViewportView(listDates);
		listDates.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		
		deleteButton.setBounds(670, 420, 115, 29);
		contentPane.add(deleteButton);
		deleteButton.setBackground(Color.LIGHT_GRAY);
		
		
		String[] columnNames = {"ID", "Distance", "Duration"};
		JTable table = new JTable(joggingData,columnNames);
		table.setRowHeight(25);
		table.setCellSelectionEnabled(true);
		table.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		table.getTableHeader().setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 22));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(374, 120, 472, 247);
		contentPane.add(scrollPane);
		
		JButton updateButton = new JButton("Update");
		updateButton.setBounds(431, 420, 115, 29);
		contentPane.add(updateButton);
		updateButton.setBackground(Color.LIGHT_GRAY);
		updateButton.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Object idJogging = table.getValueAt(row, 0);
				Object distance = table.getValueAt(row, 1);
				Object duration = table.getValueAt(row, 2);
				try {
					jogging.updateJoggingData(idJogging, distance, duration);
					joggingData = jogging.fetchJoggingDataByDate(user.getIdUser(), selectedDate);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				int len = joggingData.length;
				for(int i=0;i<len;i++) {
					for(int j=0;j<3;j++) {
						table.setValueAt(joggingData[i][j], i, j);
					}
				}
			}
		});
		
		deleteButton.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Object idJogging = table.getValueAt(row, 0);
				try {
					jogging.deleteJoggingData(idJogging);
					joggingData = jogging.fetchJoggingDataByDate(user.getIdUser(), selectedDate);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				int len = joggingData.length;
				for(int i=0;i<len;i++) {
					for(int j=0;j<3;j++) {
						table.setValueAt(joggingData[i][j], i, j);
					}
				}
			}
		});
		
		listDates.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				if (me.getClickCount() == 2) {
					selectedDate = (String) listDates.getSelectedValue();
					try {
						joggingData = jogging.fetchJoggingDataByDate(user.getIdUser(), selectedDate);
						int len = joggingData.length;
						for(int i=0;i<len;i++) {
							for(int j=0;j<3;j++) {
								table.setValueAt(joggingData[i][j], i, j);
							}
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
			}
		});
		
		JButton btnCancel = new JButton("Go back to my sports");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MySports mySports = new MySports(user);
				mySports.setVisible(true);
				dispose();
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		btnCancel.setBounds(20, 505, 231, 45);
		contentPane.add(btnCancel);
		
		JLabel lblChooseADate = new JLabel("Choose a date :");
		lblChooseADate.setForeground(Color.WHITE);
		lblChooseADate.setBounds(65, 78, 186, 20);
		lblChooseADate.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		contentPane.add(lblChooseADate);
		
		
	}
}
