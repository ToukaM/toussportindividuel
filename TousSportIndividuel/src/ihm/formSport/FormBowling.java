package ihm.formSport;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ihm.MySports;
import sportData.Bowling;
import userData.User;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class FormBowling extends JFrame {

	private JPanel contentPane;
	private JTextField textDate;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_Result;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_Spare;
	private JTextField textField_OpenFrame;
	private JTextField textField_Split;
	private JTextField textField_Strike;
	private JButton btnToday;
	private JButton btnValidate;
	private JButton btnCancel;
	private JLabel lblDetails;
	private User user;
	private Bowling bowling;
	private String date;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					FormBowling frame = new FormBowling(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormBowling(User user) {
		this.user = user;

		bowling = this.bowling;
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setContentPane(contentPane);
        setResizable(false); 

        JLabel lblError = new JLabel("Enter your Total Score");
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		lblError.setBounds(700, 98, 225, 36);
		contentPane.add(lblError);
		lblError.setVisible(false);
		
		Date current = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		date = dateFormat.format(current);
		 
		Font fontForm = new Font("Tahoma", Font.PLAIN, 20);
		Font fontLabel = new Font("Arial",Font.PLAIN,16);
		
		
		JLabel lblBowling = new JLabel("Bowling scores :");
		lblBowling.setHorizontalAlignment(SwingConstants.CENTER);
		lblBowling.setForeground(Color.WHITE);
		lblBowling.setFont(new Font("Arial Black", Font.BOLD, 35));
		lblBowling.setBounds(10, 11, 934, 62);
		contentPane.add(lblBowling);
		
		JSeparator separatorTitle = new JSeparator();
		separatorTitle.setForeground(UIManager.getColor("Button.darkShadow"));
		separatorTitle.setBounds(178, 73, 602, 2);
		contentPane.add(separatorTitle);
		

		textDate = new JTextField();
		textDate.setHorizontalAlignment(SwingConstants.CENTER);
		textDate.setBounds(300, 84, 100, 36);
		textDate.setFont(new Font("Arial", Font.BOLD, 16));
		contentPane.add(textDate);
		textDate.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(320, 197, 100, 40);
		textField_1.setFont(fontForm);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(320, 248, 100, 40);
		textField_2.setFont(fontForm);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(320, 299, 100, 40);
		textField_3.setFont(fontForm);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(320, 350, 100, 40);
		textField_4.setFont(fontForm);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(320, 401, 100, 40);
		textField_5.setFont(fontForm);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(320, 452, 100, 40);
		textField_6.setFont(fontForm);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(320, 502, 100, 40);
		textField_7.setFont(fontForm);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		textField_Result = new JTextField();
		textField_Result.setBounds(590, 95, 100, 40);
		textField_Result.setFont(fontForm);
		contentPane.add(textField_Result);
		textField_Result.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(610, 197, 100, 40);
		textField_8.setFont(fontForm);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
		
		textField_9 = new JTextField();
		textField_9.setBounds(610, 248, 100, 40);
		textField_9.setFont(fontForm);
		contentPane.add(textField_9);
		textField_9.setColumns(10);
		
		textField_Spare = new JTextField();
		textField_Spare.setBounds(610, 299, 100, 40);
		textField_Spare.setFont(fontForm);
		contentPane.add(textField_Spare);
		textField_Spare.setColumns(10);
		
		textField_OpenFrame = new JTextField();
		textField_OpenFrame.setBounds(610, 452, 100, 40);
		textField_OpenFrame.setFont(fontForm);
		contentPane.add(textField_OpenFrame);
		textField_OpenFrame.setColumns(10);
		
		textField_Split = new JTextField();
		textField_Split.setBounds(610, 401, 100, 40);
		textField_Split.setFont(fontForm);
		contentPane.add(textField_Split);
		textField_Split.setColumns(10);
		
		textField_Strike = new JTextField();
		textField_Strike.setBounds(610, 350, 100, 40);
		textField_Strike.setFont(fontForm);
		contentPane.add(textField_Strike);
		textField_Strike.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Date :");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 105, 280, 22);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("1 skittle");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(169, 205, 141, 22);
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_1.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("2 skittles");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(30, 256, 280, 22);
		lblNewLabel_2.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_2.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("3 skittles");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(30, 307, 280, 22);
		lblNewLabel_3.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_3.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("4 skittles");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_4.setBounds(30, 358, 280, 22);
		lblNewLabel_4.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_4.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("5 skittles");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_5.setBounds(30, 409, 280, 22);
		lblNewLabel_5.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_5.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("6 skittles");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_6.setBounds(30, 460, 280, 22);
		lblNewLabel_6.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_6.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("7 skittles");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_7.setBounds(30, 511, 280, 22);
		lblNewLabel_7.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_7.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Open Frame");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_8.setBounds(410, 460, 190, 22);
		lblNewLabel_8.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_8.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("Split");
		lblNewLabel_9.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_9.setBounds(410, 409, 190, 22);
		lblNewLabel_9.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_9.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("Strike");
		lblNewLabel_10.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_10.setBounds(410, 358, 190, 22);
		lblNewLabel_10.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_10.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("Spare");
		lblNewLabel_11.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_11.setBounds(410, 307, 190, 22);
		lblNewLabel_11.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_11.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_11);
		
		JLabel lblNewLabel_12 = new JLabel("9 skittles");
		lblNewLabel_12.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_12.setBounds(410, 256, 190, 22);
		lblNewLabel_12.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_12.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_12);
		
		JLabel lblNewLabel_13 = new JLabel("8 skittles");
		lblNewLabel_13.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_13.setBounds(410, 205, 190, 22);
		lblNewLabel_13.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_13.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_13);
		
		JLabel lblNewLabel_14 = new JLabel("Total Score :");
		lblNewLabel_14.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_14.setBounds(340, 105, 240, 22);
		lblNewLabel_14.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_14.setForeground(Color.WHITE);
		contentPane.add(lblNewLabel_14);
		
		btnToday = new JButton("Today");
		btnToday.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textDate.setText(date);
			}
		});
		btnToday.setBounds(300, 125, 80, 23);
		btnToday.setBackground(Color.LIGHT_GRAY);
		contentPane.add(btnToday);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.GRAY);
		separator.setBounds(10, 159, 900, 1);
		contentPane.add(separator);
		lblDetails = new JLabel(" Optional Details :");
		lblDetails.setForeground(Color.WHITE);
		lblDetails.setFont(new Font("Arial", Font.BOLD, 24));
		lblDetails.setBounds(10, 168, 208, 36);
		contentPane.add(lblDetails);
		
		btnValidate = new JButton("Validate");
		btnValidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean close = true;
				Bowling bowling = new Bowling();
				
				int one, two, three, four, five, six, seven, eight, nine, spare,  strike, openFrame,  split, result;
				one = two = three = four = five = six = seven = eight = nine = spare =  strike = openFrame =  split = 0;
				
				try {
					if(!textField_1.getText().isEmpty())one = Integer.parseInt(textField_1.getText());
					if(!textField_2.getText().isEmpty())two = Integer.parseInt(textField_2.getText());
					if(!textField_3.getText().isEmpty())three = Integer.parseInt(textField_3.getText());
					if(!textField_4.getText().isEmpty())four = Integer.parseInt(textField_4.getText());
					if(!textField_5.getText().isEmpty())five = Integer.parseInt(textField_5.getText());
					if(!textField_6.getText().isEmpty())six = Integer.parseInt(textField_6.getText());
					if(!textField_7.getText().isEmpty())seven = Integer.parseInt(textField_7.getText());
					if(!textField_8.getText().isEmpty())eight = Integer.parseInt(textField_8.getText()); 
					if(!textField_9.getText().isEmpty())nine = Integer.parseInt(textField_9.getText());
					if(!textField_Spare.getText().isEmpty())spare = Integer.parseInt(textField_Spare.getText());
					if(!textField_OpenFrame.getText().isEmpty())openFrame = Integer.parseInt(textField_OpenFrame.getText());
					if(!textField_Split.getText().isEmpty())split = Integer.parseInt(textField_Split.getText());
					if(!textField_Strike.getText().isEmpty())strike = Integer.parseInt(textField_Strike.getText());
				}catch(NumberFormatException e1) {
					lblError.setText("Enter only number");
					lblError.setVisible(true);
					close = false;
				}
				
				
				try {
				result = Integer.parseInt(textField_Result.getText());
				if(!textDate.getText().isEmpty()) date = textDate.getText();
					try {
						bowling.registerIntoDB(user, one, two, three, four, five, six, seven, eight, nine, spare,  strike, openFrame,  split, result, date);
						if(close) {
							MySports mySports = new MySports(user);
							mySports.setVisible(true);
							dispose();
						}
					} catch (SQLException e1) {
						lblError.setText("Data Base Error");
						lblError.setVisible(true);
						e1.printStackTrace();
					}
				}catch(NumberFormatException e1) {
					lblError.setVisible(true);
				}
			}
		});
		btnValidate.setBackground(Color.LIGHT_GRAY);
		btnValidate.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnValidate.setBounds(784, 505, 150, 45);
		contentPane.add(btnValidate);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MySports mySports = new MySports(user);
				mySports.setVisible(true);
				dispose();
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCancel.setBounds(20, 505, 150, 45);
		contentPane.add(btnCancel);
		
	}
}
