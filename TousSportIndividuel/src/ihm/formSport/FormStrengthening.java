package ihm.formSport;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ihm.MySports;
import sportData.LongJump;
import sportData.Strengthening;
import userData.User;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class FormStrengthening extends JFrame {

	private JPanel contentPane;
	private User user;
	private JButton btnValidate;
	private JTextField textField_Number;
	private JButton btnToday;
	private JComboBox comboBox;
	private String date;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormStrengthening frame = new FormStrengthening(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormStrengthening(User user) {
		this.user = user;
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		setResizable(false);
		
		JLabel lblStrengthening = new JLabel("Strengthening scores :");
		lblStrengthening.setHorizontalAlignment(SwingConstants.CENTER);
		lblStrengthening.setForeground(Color.WHITE);
		lblStrengthening.setFont(new Font("Arial Black", Font.BOLD, 35));
		lblStrengthening.setBounds(10, 11, 934, 62);
		contentPane.add(lblStrengthening);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(UIManager.getColor("Button.darkShadow"));
		separator.setBounds(175, 84, 602, 2);
		contentPane.add(separator);
		
		JLabel lblError = new JLabel("Enter a number");
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		lblError.setBounds(306, 122, 341, 36);
		contentPane.add(lblError);
		lblError.setVisible(false);
		
		Date current = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		date = dateFormat.format(current);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		lblDate.setBounds(375, 399, 210, 36);
		contentPane.add(lblDate);
		
		JTextField textDate = new JTextField();
		textDate.setHorizontalAlignment(SwingConstants.CENTER);
		textDate.setBounds(430, 437, 100, 36);
		textDate.setFont(new Font("Arial", Font.BOLD, 16));
		contentPane.add(textDate);
		textDate.setColumns(10);
		
		btnToday = new JButton("Today");
		btnToday.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textDate.setText(date);
			}
		});
		btnToday.setBounds(440, 484, 80, 23);
		btnToday.setBackground(Color.LIGHT_GRAY);
		contentPane.add(btnToday);
		
		textField_Number = new JTextField();
		textField_Number.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		textField_Number.setHorizontalAlignment(SwingConstants.CENTER);
		textField_Number.setBounds(375, 207, 210, 45);
		contentPane.add(textField_Number);

		JLabel lbl_Number = new JLabel("Number");
		lbl_Number.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Number.setForeground(Color.WHITE);
		lbl_Number.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		lbl_Number.setBounds(375, 169, 210, 36);
		contentPane.add(lbl_Number);
		
		JLabel lbl_Duration = new JLabel("Type of movement");
		lbl_Duration.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Duration.setForeground(Color.WHITE);
		lbl_Duration.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		lbl_Duration.setBounds(375, 300, 210, 36);
		contentPane.add(lbl_Duration);
		
		String[] movementType = {"Abs","Push up","Squats"};
		comboBox = new JComboBox(movementType);
		comboBox.setBackground(Color.WHITE);
		comboBox.setForeground(Color.BLACK);
		comboBox.setFont(new Font("Arial", Font.PLAIN, 20));
		comboBox.setBounds(419, 337, 121, 36);
		contentPane.add(comboBox);
		comboBox.setSelectedIndex(1);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MySports mySports = new MySports(user);
				mySports.setVisible(true);
				dispose();
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCancel.setBounds(20, 505, 150, 45);
		contentPane.add(btnCancel);
		
		btnValidate = new JButton("Validate");
		btnValidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Strengthening strengthening = new Strengthening();
				String movementType = "";
				int number = 0;
				try {
					number = Integer.parseInt(textField_Number.getText());
				}catch(NumberFormatException e2) {
					lblError.setVisible(true);
				}
				
			    movementType = (String)comboBox.getSelectedItem();
			    switch (movementType) {
					case "Abs": movementType="Abs";
						break;
					case"Push up": movementType="Push-up";
						break;
					case"Squats": movementType="Squats";
						break;
				}
				
				try {
					if(!textDate.getText().isEmpty()) date = textDate.getText();
					if((number > 0) && (movementType!="")) {
						strengthening.registerIntoDB(user,number,movementType,date);
						MySports mySports = new MySports(user);
						mySports.setVisible(true);
						dispose();
					}
					else{
						lblError.setVisible(true);
					}
				} catch (SQLException e1) {
					lblError.setText("Connection to Serveur lost");
					lblError.setVisible(true);
					e1.printStackTrace();
				}
				
				
			}
		});
		btnValidate.setBackground(Color.LIGHT_GRAY);
		btnValidate.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnValidate.setBounds(784, 505, 150, 45);
		contentPane.add(btnValidate);
		
	}
}
