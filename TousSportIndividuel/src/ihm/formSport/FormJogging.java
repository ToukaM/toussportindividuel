package ihm.formSport;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import ihm.MySports;
import sportData.Bowling;
import sportData.Jogging;
import userData.User;
import javax.swing.JSeparator;

public class FormJogging extends JFrame {

	private JPanel contentPane;
	private User user;
	private JButton btnValidate;
	private JTextField textField_Distance;
	private JTextField textField_Duration;
	private JButton btnToday;
	private String date;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormJogging frame = new FormJogging(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormJogging(User user) {
		this.user = user;
		
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false); 
		setContentPane(contentPane);
		
		JLabel lblJogging = new JLabel("Jogging scores :");
		lblJogging.setHorizontalAlignment(SwingConstants.CENTER);
		lblJogging.setForeground(Color.WHITE);
		lblJogging.setFont(new Font("Arial Black", Font.BOLD, 35));
		lblJogging.setBounds(10, 11, 934, 62);
		contentPane.add(lblJogging);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(UIManager.getColor("Button.darkShadow"));
		separator.setBounds(175, 84, 602, 2);
		contentPane.add(separator);
		
		JLabel lblError = new JLabel("Enter the distance and the duration");
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		lblError.setBounds(10, 122, 934, 36);
		contentPane.add(lblError);
		lblError.setVisible(false);
		
		Date current = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		date = dateFormat.format(current);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		lblDate.setBounds(375, 399, 210, 36);
		contentPane.add(lblDate);
		
		JTextField textDate = new JTextField();
		textDate.setHorizontalAlignment(SwingConstants.CENTER);
		textDate.setBounds(430, 437, 100, 36);
		textDate.setFont(new Font("Arial", Font.BOLD, 16));
		contentPane.add(textDate);
		textDate.setColumns(10);
		
		btnToday = new JButton("Today");
		btnToday.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textDate.setText(date);
			}
		});
		btnToday.setBounds(440, 484, 80, 23);
		btnToday.setBackground(Color.LIGHT_GRAY);
		contentPane.add(btnToday);
		
		textField_Distance = new JTextField();
		textField_Distance.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		textField_Distance.setHorizontalAlignment(SwingConstants.CENTER);
		textField_Distance.setBounds(375, 207, 210, 45);
		contentPane.add(textField_Distance);
		
		textField_Duration = new JTextField();
		textField_Duration.setHorizontalAlignment(SwingConstants.CENTER);
		textField_Duration.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		textField_Duration.setBounds(375, 328, 210, 45);
		contentPane.add(textField_Duration);
		
		JLabel lbl_Distance = new JLabel("Distance");
		lbl_Distance.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Distance.setForeground(Color.WHITE);
		lbl_Distance.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		lbl_Distance.setBounds(375, 169, 210, 36);
		contentPane.add(lbl_Distance);
		
		JLabel lbl_Duration = new JLabel("Duration");
		lbl_Duration.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Duration.setForeground(Color.WHITE);
		lbl_Duration.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		lbl_Duration.setBounds(375, 291, 210, 36);
		contentPane.add(lbl_Duration);
		
		JLabel lbl_meter = new JLabel("in meter :");
		lbl_meter.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_meter.setForeground(Color.WHITE);
		lbl_meter.setFont(new Font("Arial", Font.PLAIN, 18));
		lbl_meter.setBounds(253, 225, 112, 22);
		contentPane.add(lbl_meter);
		
		JLabel lbl_minute = new JLabel("in minute :");
		lbl_minute.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_minute.setForeground(Color.WHITE);
		lbl_minute.setFont(new Font("Arial", Font.PLAIN, 18));
		lbl_minute.setBounds(253, 346, 112, 22);
		contentPane.add(lbl_minute);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					MySports mySports = new MySports(user);
				mySports.setVisible(true);
				dispose();
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCancel.setBounds(20, 505, 150, 45);
		contentPane.add(btnCancel);
		
		
		btnValidate = new JButton("Validate");
		btnValidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Jogging jogging = new Jogging();
				int distance=0, duration=0;
				
				try {
					distance = Integer.parseInt(textField_Distance.getText());
					duration = Integer.parseInt(textField_Duration.getText());
				}catch(NumberFormatException e1) {
					lblError.setVisible(true);
				}
				try {
					if(!textDate.getText().isEmpty()) date = textDate.getText();
					if((distance > 0) && (duration > 0)) {
						jogging.registerIntoDB(user, distance, duration, date);
						MySports mySports = new MySports(user);
						mySports.setVisible(true);
						dispose();
					}
				} catch (SQLException e) {
					lblError.setText("Data Base Error");
					lblError.setVisible(true);
					e.printStackTrace();
				}
			}
		});
		btnValidate.setBackground(Color.LIGHT_GRAY);
		btnValidate.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnValidate.setBounds(784, 505, 150, 45);
		contentPane.add(btnValidate);
		


	}
}
