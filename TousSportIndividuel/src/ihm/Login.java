package ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import userData.User;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Login extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField mailField;
	private JPasswordField passwordField;
	private JButton btnSignUp;
	private JButton btnLogin;
	private JLabel lblWrongPass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		this.setSize(650, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false); 
		
		btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		btnLogin.setBackground(Color.GRAY);
		btnLogin.setBounds(164, 353, 120, 37);
		contentPane.add(btnLogin);
		btnLogin.addActionListener(this);
		
		mailField = new JTextField();
		mailField.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		mailField.setHorizontalAlignment(SwingConstants.CENTER);
		mailField.setBounds(164, 188, 272, 48);
		contentPane.add(mailField);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setBounds(164, 276, 272, 48);
		contentPane.add(passwordField);
		
		btnSignUp = new JButton("Sign up");
		btnSignUp.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		btnSignUp.setBackground(Color.GRAY);
		btnSignUp.setBounds(316, 353, 120, 36);
		contentPane.add(btnSignUp);
		btnSignUp.addActionListener(this);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblMail.setHorizontalAlignment(SwingConstants.CENTER);
		lblMail.setForeground(Color.WHITE);
		lblMail.setBounds(164, 161, 272, 26);
		contentPane.add(lblMail);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(164, 248, 272, 16);
		contentPane.add(lblPassword);
		
		JLabel lblTousSportIndividuel = new JLabel("TOUS SPORTS INDIVIDUELS");
		lblTousSportIndividuel.setForeground(Color.WHITE);
		lblTousSportIndividuel.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		lblTousSportIndividuel.setBounds(140, 76, 400, 73);
		contentPane.add(lblTousSportIndividuel);
		
		lblWrongPass = new JLabel("Wrong password or email, please try again");
		lblWrongPass.setForeground(Color.RED);
		lblWrongPass.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		lblWrongPass.setBounds(120, 34, 374, 30);
		contentPane.add(lblWrongPass);
		lblWrongPass.setVisible(false);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==btnSignUp) {
			Signup signup = new Signup();
			signup.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==btnLogin) {
			if((!mailField.getText().equals(""))&&(!passwordField.getText().equals(""))) {
				String mail = mailField.getText();
				String password = passwordField.getText();
				User user = new User();
				try {
					user.login(mail, password);
				} catch (SQLException e) {
					System.err.println("Unable to access the database");	
					e.printStackTrace();
				}
				if(user.getIdUser()==0) {
					lblWrongPass.setVisible(true);
				}
				else {
					UserWall userWall = new UserWall(user);
					userWall.setVisible(true);
					this.dispose();
				}
			}
		}
		
	}
}
