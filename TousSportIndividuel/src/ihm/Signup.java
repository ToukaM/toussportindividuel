package ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import userData.User;

import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SpinnerListModel;

public class Signup extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField firstName;
	private JPasswordField passwordField;
	private JTextField lastName;
	private JTextField mail;
	private JButton btnSignUp;
	private JButton btnLogin;
	private JSpinner gender;
	private JLabel lblMailExists;
	private JLabel lblfieldsEmpty;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Signup frame = new Signup();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Signup() {
		this.setSize(650, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false); 
		
		firstName = new JTextField();
		firstName.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		firstName.setHorizontalAlignment(SwingConstants.CENTER);
		firstName.setBounds(164, 154, 272, 48);
		contentPane.add(firstName);
		firstName.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setBounds(164, 429, 272, 48);
		contentPane.add(passwordField);
		
		btnSignUp = new JButton("Sign up");
		btnSignUp.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		btnSignUp.setBackground(Color.GRAY);
		btnSignUp.setBounds(316, 627, 120, 36);
		contentPane.add(btnSignUp);
		btnSignUp.addActionListener(this);
		
		btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		btnLogin.setBackground(Color.GRAY);
		btnLogin.setBounds(164, 627, 120, 37);
		contentPane.add(btnLogin);
		btnLogin.addActionListener(this);
		
		JLabel lblName = new JLabel("First name");
		lblName.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setForeground(Color.WHITE);
		lblName.setBounds(164, 121, 272, 26);
		contentPane.add(lblName);
		
		JLabel lblFamilyName = new JLabel("Last name");
		lblFamilyName.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblFamilyName.setHorizontalAlignment(SwingConstants.CENTER);
		lblFamilyName.setForeground(Color.WHITE);
		lblFamilyName.setBounds(164, 214, 272, 26);
		contentPane.add(lblFamilyName);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setBounds(164, 307, 272, 26);
		contentPane.add(lblEmail);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(164, 400, 272, 16);
		contentPane.add(lblPassword);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lblGender.setHorizontalAlignment(SwingConstants.CENTER);
		lblGender.setForeground(Color.WHITE);
		lblGender.setBounds(164, 493, 272, 16);
		contentPane.add(lblGender);
		
		JLabel lblSignUp = new JLabel("SIGN UP");
		lblSignUp.setForeground(Color.WHITE);
		lblSignUp.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		lblSignUp.setBounds(250, 6, 120, 48);
		contentPane.add(lblSignUp);
		
		lastName = new JTextField();
		lastName.setBounds(164, 243, 272, 48);
		lastName.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		lastName.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lastName);
		lastName.setColumns(10);
		
		mail = new JTextField();
		mail.setBounds(164, 336, 272, 48);
		mail.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		mail.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(mail);
		mail.setColumns(10);
		
		gender = new JSpinner();
		gender.setModel(new SpinnerListModel(new String[] {"Male", "Female"}));
		gender.setBounds(164, 522, 272, 48);
		gender.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		contentPane.add(gender);
		
		lblMailExists = new JLabel("This mail adress already exists, please use another one");
		lblMailExists.setForeground(Color.RED);
		lblMailExists.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		lblMailExists.setBounds(76, 61, 465, 48);
		contentPane.add(lblMailExists);
		lblMailExists.setVisible(false);
		
		lblfieldsEmpty = new JLabel("Please fill all the fields");
		lblfieldsEmpty.setForeground(Color.RED);
		lblfieldsEmpty.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
		lblfieldsEmpty.setBounds(214, 61, 196, 48);
		contentPane.add(lblfieldsEmpty);
		lblfieldsEmpty.setVisible(false);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==btnSignUp) {
			if((!firstName.getText().equals(""))&&(!passwordField.getText().equals(""))&&(!mail.getText().equals(""))&&(!lastName.getText().equals(""))) {
				User user = new User();
				boolean mailexist=true;
				try {
					mailexist = user.verifyEmail(mail.getText());
				} catch (SQLException e) {
					System.err.println("The email exists already try to login");
					e.printStackTrace();
				}
				if(mailexist==false) {
					user.signUp(firstName.getText(), lastName.getText(), gender.getValue().toString(), mail.getText(), passwordField.getText());
					Login login = new Login();
					login.setVisible(true);
					this.dispose();
				}
				else {
					lblMailExists.setVisible(true);
				}
			}
			else {
				lblfieldsEmpty.setVisible(true);
			}
		}
		if(arg0.getSource()==btnLogin) {
			Login login = new Login();
			login.setVisible(true);
			this.dispose();
		}
		
	}
}
