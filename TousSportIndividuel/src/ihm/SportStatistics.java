package ihm;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.xy.XYDataset;

import chart.Graph;
import userData.User;
import javax.swing.border.LineBorder;

public class SportStatistics extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton userWallButton;
	private JButton mySportsButton;
	private JButton sportStatisticsButton;
	private JButton searchButton;
	private JButton logoutButton;
	private JButton saveButton;
	private User user;
	private JList listSports;
	private JList listFriends;
	private static User friend = new User(0);
	private static JFreeChart chart;
	private JFreeChart friendChart;
	private static String selectedSport;
	private Graph graph;
	private final JFileChooser fileChooser = new JFileChooser();

	/**
	 * Launch the application.
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com", "azerty");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SportStatistics frame = new SportStatistics(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public SportStatistics(User user) throws SQLException {
		this.user = user;
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		setResizable(false); 
		
		Font fontInfos = new Font("Arial",Font.PLAIN,16);
		Font fontButton = new Font("Microsoft YaHei UI Light", Font.BOLD, 18);
		
		
		/*--------Chart panel------------*/
		
		JPanel chartsPanel = new JPanel();
		chartsPanel.setBounds(200, 95, 530, 370);
		chartsPanel.setVisible(false);
		contentPane.setLayout(null);
		contentPane.add(chartsPanel);
		
		graph = new Graph();
		ChartPanel chartPanel = new ChartPanel(chart);
		saveButton = new JButton("save");
		saveButton.setBounds(0, 341, 100, 29);
		saveButton.setFont(fontButton);
		saveButton.setBackground(Color.LIGHT_GRAY);
		saveButton.addActionListener(this);
		chartsPanel.setLayout(null);
		chartPanel.setLayout(null);
		chartsPanel.add(saveButton);
		chartsPanel.add(chartPanel);
		
		ChartPanel friendChartPanel = new ChartPanel(friendChart);
		friendChartPanel.setBounds(265, 0, 265, 336);
		chartsPanel.add(friendChartPanel);
		friendChartPanel.setVisible(false);
		

		/*--------Sports list---------*/
		
		JLabel lblSports = new JLabel("Sports :");
		lblSports.setBounds(20, 52, 195, 32);
		lblSports.setForeground(Color.WHITE);
		lblSports.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		contentPane.add(lblSports);
		
		ArrayList<String> sports = new ArrayList<>();
		sports = user.getSports();
		JScrollPane scrollPaneSports = new JScrollPane();
		scrollPaneSports.setBounds(20, 95, 147, 336);
		contentPane.add(scrollPaneSports);
		listSports = new JList(sports.toArray());
		listSports.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPaneSports.setViewportView(listSports);
		listSports.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listSports.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	            if (me.getClickCount() == 1) {
	            	selectedSport = (String) listSports.getSelectedValue();
	            		try {
	            			chart = graph.createChart(selectedSport, user.getIdUser(), 0);
						} catch (SQLException e) {
							e.printStackTrace();
						}
	            		chartPanel.setBounds(0, 0, 530, 336);
	            		chartPanel.setChart(chart);
	            		chartsPanel.setVisible(true);
	            	
	            }
	         }
		});
		

		/*--------Friends list--------*/
		
		JLabel lblFriendsList = new JLabel("Friends list :");
		lblFriendsList.setBounds(761, 52, 164, 32);
		lblFriendsList.setForeground(Color.WHITE);
		lblFriendsList.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		contentPane.add(lblFriendsList);
		
		ArrayList<String> friends = new ArrayList<>();
		friends = user.getNameFriendList();
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(761, 95, 164, 336);
		contentPane.add(scrollPane);
		listFriends = new JList(friends.toArray());
		listFriends.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setViewportView(listFriends);
		listFriends.setFont(new Font("Tahoma", Font.PLAIN, 16));
		listFriends.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	            if (me.getClickCount() == 1) {
	            	String selected = (String) listFriends.getSelectedValue();
	            	String names[] = selected.split(" ");
	            	try {
						int userID = user.fetchUserID(names[0], names[1]);
						friend = user.fetchUserData(userID);
						if((selectedSport.equals("Swimming"))||(selectedSport.equals("Strengthening"))) {
							chart = graph.createChart(selectedSport, user.getIdUser(),friend.getIdUser());
							friendChart = graph.createFriendChart(selectedSport, friend.getIdUser());
							chartPanel.setBounds(0, 0, 265, 336);
							chartPanel.setChart(chart);
							friendChartPanel.setChart(friendChart);
							friendChartPanel.setVisible(true);
						}
						else {
							chart = graph.createChart(selectedSport, user.getIdUser(),friend.getIdUser());
							chartPanel.setBounds(0, 0, 530, 336);
							chartPanel.setChart(chart);
						}
						
	            		chartsPanel.setVisible(true);
					} catch (SQLException e) {
						
						e.printStackTrace();
					}
	            }
	         }
		});
		
		
		/*-------------navigation buttons-----------------*/
		
		userWallButton = new JButton("My Wall");
		userWallButton.setBounds(20, 500, 150, 45);
		userWallButton.setFont(fontButton);
		userWallButton.setBackground(Color.LIGHT_GRAY);
		contentPane.add(userWallButton);
		userWallButton.addActionListener(this);
		
		mySportsButton = new JButton("My Sports");
		mySportsButton.setBounds(190, 500, 150, 45);
		mySportsButton.setFont(fontButton);
		mySportsButton.setBackground(Color.LIGHT_GRAY);
		contentPane.add(mySportsButton);
		mySportsButton.addActionListener(this);
		
		sportStatisticsButton = new JButton("Sport Stats");
		sportStatisticsButton.setBounds(360, 500, 150, 45);
		sportStatisticsButton.setFont(fontButton);
		sportStatisticsButton.setBackground(Color.LIGHT_GRAY);
		contentPane.add(sportStatisticsButton);
		sportStatisticsButton.setEnabled(false);
		
		searchButton = new JButton("Search");
		searchButton.setBounds(530, 500, 150, 45);
		searchButton.setFont(fontButton);
		searchButton.setBackground(Color.LIGHT_GRAY);
		contentPane.add(searchButton);
		searchButton.addActionListener(this);
		
		logoutButton = new JButton("Logout");
		logoutButton.setBounds(817, 500, 116, 45);
		logoutButton.setFont(fontButton);
		logoutButton.setBackground(Color.LIGHT_GRAY);
		contentPane.add(logoutButton);
		logoutButton.addActionListener(this);
		
		
	}

	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==userWallButton) {
			UserWall userWall = new UserWall(user);
			userWall.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==mySportsButton) {
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==searchButton) {
			Search search = new Search(user);
			search.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==logoutButton) {
			Login login = new Login();
			login.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==saveButton) {
			String path = null;
			int returnVal = fileChooser.showOpenDialog(contentPane);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File file = fileChooser.getSelectedFile();
	            path = file.getPath();
	        } 
			graph.saveChart(chart,path);
		}

	}
}
