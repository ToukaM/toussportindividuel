package ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import userData.User;

import javax.swing.JButton;

public class FriendUserWall extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel label_FirstName;
	private JLabel label_LastName;
	private JLabel label_age;
	private JLabel label_Gender;
	private JButton btnFriendRequest;
	private User user;
	private User friend;
	private JList<String> sports;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();
		User friend = new User();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FriendUserWall frame = new FriendUserWall(friend,user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public FriendUserWall(User friend, User user) throws SQLException {
		this.user = user;
		this.friend = friend;
		this.setSize(670, 470);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);
		setContentPane(contentPane);
		
		Font fontInfos = new Font("Arial",Font.PLAIN,16);
		Font fontButton = new Font("Microsoft YaHei UI Light", Font.BOLD, 18);
		
		JLabel lblInformations = new JLabel("Informations :");
		lblInformations.setForeground(Color.WHITE);
		lblInformations.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		lblInformations.setBounds(58, 48, 171, 32);
		contentPane.add(lblInformations);
		
		label_FirstName = new JLabel();
		label_FirstName.setText(friend.getFirstName());
		label_FirstName.setBackground(Color.DARK_GRAY);
		label_FirstName.setForeground(Color.WHITE);
		label_FirstName.setFont(fontInfos);
		label_FirstName.setBounds(58, 130, 140, 20);
		contentPane.add(label_FirstName);
		
		label_LastName = new JLabel();
		label_LastName.setText(friend.getLastName());
		label_LastName.setBackground(Color.DARK_GRAY);
		label_LastName.setForeground(Color.WHITE);
		label_LastName.setFont(fontInfos);
		label_LastName.setBounds(58, 170, 140, 20);
		contentPane.add(label_LastName);
		
		label_age = new JLabel();
		label_age.setText(friend.getAge()+"");
		label_age.setBackground(Color.DARK_GRAY);
		label_age.setForeground(Color.WHITE);
		label_age.setFont(fontInfos);
		label_age.setBounds(58, 210, 140, 20);
		contentPane.add(label_age);
		
		label_Gender = new JLabel();
		label_Gender.setText(friend.getSexe());
		label_Gender.setBackground(Color.DARK_GRAY);
		label_Gender.setForeground(Color.WHITE);
		label_Gender.setFont(fontInfos);
		label_Gender.setBounds(58, 250, 140, 20);
		contentPane.add(label_Gender);
		
		btnFriendRequest = new JButton("Send friend request");
		contentPane.add(btnFriendRequest);
		btnFriendRequest.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnFriendRequest.setBackground(Color.LIGHT_GRAY);
		btnFriendRequest.setBounds(58, 303, 200, 32);
		btnFriendRequest.setVisible(false);
		
		
		boolean accepted = false;
		accepted = user.verifyFriendShip(user.getIdUser(), friend.getIdUser());
		if(!accepted) {
			btnFriendRequest.setVisible(true);
			btnFriendRequest.addActionListener(this);
		}
		
		boolean sent = false;
		sent = user.verifyFriendRequest(user.getIdUser(), friend.getIdUser());
		if(sent) {
			btnFriendRequest.setText("Friend request sent");
			btnFriendRequest.setEnabled(false);
			
		}
		
		JLabel sportLabel = new JLabel("Sports list");
		sportLabel.setBackground(Color.DARK_GRAY);
		sportLabel.setForeground(Color.WHITE);
		sportLabel.setFont(fontInfos);
		sportLabel.setBounds(400, 70, 140, 20);
		contentPane.add(sportLabel);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(400, 95, 214, 280);
		contentPane.add(scrollPane);
				
		sports = new JList(friend.getSports().toArray());
		sports.setBounds(400, 96, 214, 278);
		scrollPane.setViewportView(sports);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==btnFriendRequest) {
			try {
				user.sendFriendRequest(friend.getIdUser());
				btnFriendRequest.setText("Friend request sent");
				btnFriendRequest.setEnabled(false);
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
	}
}
