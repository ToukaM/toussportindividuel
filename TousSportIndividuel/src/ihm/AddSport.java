package ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

// import sportData.*;

import userData.User;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class AddSport extends JFrame implements ActionListener {

	private JPanel contentPane;
	private User user;
	
	/*
	private Bowling bowling;
	private Jogging jogging;
	private LongJump longJump;
	private	ShotPut shotPut;
	private Strengthening strengthening;
	private Swimming swimming;
	*/
	private JButton btnBowling;
	private JButton btnJogging;
	private JButton btnLongjump;
	private JButton btnShotput;
	private JButton btnStrengthening;
	private JButton btnSwimming;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddSport frame = new AddSport(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddSport(User user) {
		this.user = user;
		this.setSize(960, 600);
		ArrayList<String> sportList = new ArrayList<String>();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		setResizable(false); 
		contentPane.setLayout(null);
		
		
		boolean btnBowlingVisible = true;
		boolean btnJoggingVisible = true;
		boolean btnLongjumpVisible = true;
		boolean btnShotputVisible = true;
		boolean btnStrengtheningVisible = true;
		boolean btnSwimmingVisible = true;
		int verticalPos = 120;
		
		sportList = user.getSports();
		
		for (String sport : sportList) {	
			switch (sport){
			 	case "Bowling":  
			 		btnBowlingVisible = false;
			 		break;	
			 	case "Jogging":  
			 		btnJoggingVisible = false;
			 		break;	
			 	case "LongJump":  
			 		btnLongjumpVisible = false;
			 		break;
			 	case "ShotPut":  
			 		btnShotputVisible = false;
			 		break;
			 	case "Strengthening":  
			 		btnStrengtheningVisible = false;
			 		break;
			 	case "Swimming":  
			 		 btnSwimmingVisible = false;
			 		break;
			 } 
		}
		if(btnBowlingVisible) {
			btnBowling = new JButton("Bowling");
	 		btnBowling.setBackground(Color.LIGHT_GRAY);
	 		btnBowling.setBounds(380, verticalPos, 200, 40);
			contentPane.add(btnBowling);
			btnBowling.addActionListener(this);
			verticalPos += 60;
		}
		if(btnJoggingVisible) {
			btnJogging = new JButton("Jogging");
			btnJogging.setBackground(Color.LIGHT_GRAY);
			btnJogging.setBounds(380, verticalPos, 200, 40);
			contentPane.add(btnJogging);
			btnJogging.addActionListener(this);
			verticalPos += 60;
		}
		
		if(btnLongjumpVisible) {	
			btnLongjump = new JButton("LongJump");
			btnLongjump.setBackground(Color.LIGHT_GRAY);
			btnLongjump.setBounds(380,verticalPos, 200, 40);
			contentPane.add(btnLongjump);
			btnLongjump.addActionListener(this);
			verticalPos += 60;
		}
		if(btnShotputVisible) {
			btnShotput = new JButton("ShotPut");
			btnShotput.setBackground(Color.LIGHT_GRAY);
			btnShotput.setBounds(380,verticalPos, 200, 40);
			contentPane.add(btnShotput);
			btnShotput.addActionListener(this);
			verticalPos += 60;
		}
		if(btnStrengtheningVisible) {
			btnStrengthening = new JButton("Strengthening");
			btnStrengthening.setBackground(Color.LIGHT_GRAY);
			btnStrengthening.setBounds(380,verticalPos, 200, 40);
			contentPane.add(btnStrengthening);
			btnStrengthening.addActionListener(this);
			verticalPos += 60;
		}
		if(btnSwimmingVisible) {
			btnSwimming = new JButton("Swimming");
			btnSwimming.setBackground(Color.LIGHT_GRAY);
			btnSwimming.setBounds(380,verticalPos, 200, 40);
			contentPane.add(btnSwimming);
			btnSwimming.addActionListener(this);
		}	
			

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MySports mySports = new MySports(user);
				mySports.setVisible(true);
				dispose();
				
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBack.setBackground(Color.LIGHT_GRAY);
		btnBack.setBounds(10, 11, 74, 39);
		contentPane.add(btnBack);
		
		JLabel lblNewSport = new JLabel("New Sport :");
		lblNewSport.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewSport.setForeground(Color.WHITE);
		lblNewSport.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 30));
		lblNewSport.setBounds(10, 54, 924, 39);
		contentPane.add(lblNewSport);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==btnBowling) {
			try {
				user.addSport("Bowling");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==btnJogging) {
			try {
				user.addSport("Jogging");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==btnLongjump) {
			try {
				user.addSport("LongJump");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==btnShotput) {
			try {
				user.addSport("ShotPut");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==btnStrengthening) {
			try {
				user.addSport("Strengthening");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==btnSwimming) {
			try {
				user.addSport("Swimming");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
	}
}
