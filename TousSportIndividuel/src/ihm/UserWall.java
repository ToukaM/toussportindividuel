package ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


import userData.User;

import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

import java.awt.GridBagLayout;
import java.awt.Scrollbar;

import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JFrame;
import ihm.AchievementPanel;

public class UserWall extends JFrame implements ActionListener{

	private JPanel contentPane;
	private AchievementPanel achievement;
	private JTable table;
	private JTextField text_FirstName;
	private JTextField text_LastName;
	private JTextField text_age;
	private JTextField text_Gender;
	private JTextField text_OldPassword;
	private JTextField text_NewPassword;
	private ArrayList<String> nameFriends = new ArrayList <String>();
	private ArrayList<Integer> friendRequests;
	private HashMap<String,Integer> usersSender;
	private JButton userWallButton;
	private JButton mySportsButton;
	private JButton sportStatisticsButton;
	private JButton searchButton;
	private JButton logoutButton;
	private JButton btnUpdate;
	private JButton btnValidUpdate;
	private JButton btnCancelUpdate;
	private JButton btnAccept;
	private JButton btnClose;
	private User user;
	private JLabel lblOldPassword;
	private JLabel lblNewPassword;
	private JLabel lblMessage;
	private JList listFriends;
	private JList listFriendRequests;
	private JPanel friendRequestsPanel;
	private int userIDClicked;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserWall frame = new UserWall(user);
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	} 

	/**
	 * Create the frame.
	 */
	public UserWall(User user){
		this.user = user;
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		setResizable(false);
		
		Font fontLittleButton = new Font("Tahoma", Font.PLAIN, 14);
		Font fontInfos = new Font("Arial",Font.PLAIN,16);
		Font fontButton = new Font("Microsoft YaHei UI Light", Font.BOLD, 18);
		
		JLabel lblInformations = new JLabel("Informations :");
		lblInformations.setForeground(Color.WHITE);
		lblInformations.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		lblInformations.setBounds(20, 44, 171, 32);
		contentPane.add(lblInformations);
		
		
	//NAVIGATE BUTTONS
		
		
		userWallButton = new JButton("My Wall");
		userWallButton.setFont(fontButton);
		userWallButton.setBackground(Color.LIGHT_GRAY);
		userWallButton.setBounds(20, 500, 150, 45);
		contentPane.add(userWallButton);
		userWallButton.setEnabled(false);
		
		mySportsButton = new JButton("My Sports");
		mySportsButton.setFont(fontButton);
		mySportsButton.setBackground(Color.LIGHT_GRAY);
		mySportsButton.setBounds(190, 500, 150, 45);
		contentPane.add(mySportsButton);
		mySportsButton.addActionListener(this);
		
		sportStatisticsButton = new JButton("Sport Stats");
		sportStatisticsButton.setFont(fontButton);
		sportStatisticsButton.setBackground(Color.LIGHT_GRAY);
		sportStatisticsButton.setBounds(360, 500, 150, 45);
		contentPane.add(sportStatisticsButton);
		sportStatisticsButton.addActionListener(this);
		
		searchButton = new JButton("Search");
		searchButton.setFont(fontButton);
		searchButton.setBackground(Color.LIGHT_GRAY);
		searchButton.setBounds(530, 500, 150, 45);
		contentPane.add(searchButton);
		searchButton.addActionListener(this);
		
		logoutButton = new JButton("Logout");
		logoutButton.setFont(fontButton);
		logoutButton.setBackground(Color.LIGHT_GRAY);
		logoutButton.setBounds(817, 500, 116, 45);
		contentPane.add(logoutButton);
		logoutButton.addActionListener(this);
		
		
	//INFORMATIONS AND UPDATES
		
		
		text_FirstName = new JTextField();
		text_FirstName.setHorizontalAlignment(SwingConstants.CENTER);
		text_FirstName.setText(user.getFirstName());
		text_FirstName.setBackground(Color.DARK_GRAY);
		text_FirstName.setForeground(Color.WHITE);
		text_FirstName.setCaretColor(Color.WHITE);
		text_FirstName.setFont(new Font("Arial", Font.PLAIN, 20));
		text_FirstName.setBounds(20, 123, 180, 36);
		contentPane.add(text_FirstName);
		text_FirstName.setEditable(false);
		
		text_LastName = new JTextField();
		text_LastName.setHorizontalAlignment(SwingConstants.CENTER);
		text_LastName.setText(user.getLastName());
		text_LastName.setBackground(Color.DARK_GRAY);
		text_LastName.setForeground(Color.WHITE);
		text_LastName.setCaretColor(Color.WHITE);
		text_LastName.setFont(new Font("Arial", Font.PLAIN, 20));
		text_LastName.setBounds(20, 170, 180, 36);
		contentPane.add(text_LastName);
		text_LastName.setEditable(false);
		
		text_age = new JTextField("");
		text_age.setHorizontalAlignment(SwingConstants.CENTER);
		String age;
		if(user.getAge()==0) {
			age = "Age";
		}
		else {
			age = user.getAge()+"";
		}
		
		text_age.setText(age);
		text_age.setBackground(Color.DARK_GRAY);
		text_age.setForeground(Color.WHITE);
		text_age.setCaretColor(Color.WHITE);
		text_age.setFont(new Font("Arial", Font.PLAIN, 20));
		text_age.setBounds(20, 217, 180, 36);
		contentPane.add(text_age);
		text_age.setEditable(false);
		
		text_Gender = new JTextField();
		text_Gender.setHorizontalAlignment(SwingConstants.CENTER);
		text_Gender.setText(user.getSexe());
		text_Gender.setBackground(Color.DARK_GRAY);
		text_Gender.setForeground(Color.WHITE);
		text_Gender.setCaretColor(Color.WHITE);
		text_Gender.setFont(new Font("Arial", Font.PLAIN, 20));
		text_Gender.setBounds(20, 264, 180, 36);
		contentPane.add(text_Gender);
		text_Gender.setEditable(false);
		
		text_OldPassword = new JTextField();
		text_OldPassword.setHorizontalAlignment(SwingConstants.CENTER);
		text_OldPassword.setBackground(Color.DARK_GRAY);
		text_OldPassword.setForeground(Color.WHITE);
		text_OldPassword.setCaretColor(Color.WHITE);
		text_OldPassword.setFont(new Font("Arial", Font.PLAIN, 20));
		text_OldPassword.setBounds(20, 374, 180, 36);
		contentPane.add(text_OldPassword);
		text_OldPassword.setVisible(false);
		
		text_NewPassword = new JTextField();
		text_NewPassword.setHorizontalAlignment(SwingConstants.CENTER);
		text_NewPassword.setBackground(Color.DARK_GRAY);
		text_NewPassword.setForeground(Color.WHITE);
		text_NewPassword.setCaretColor(Color.WHITE);
		text_NewPassword.setFont(new Font("Arial", Font.PLAIN, 20));
		text_NewPassword.setBounds(20, 301, 180, 36);
		contentPane.add(text_NewPassword);
		text_NewPassword.setVisible(false);
		
		btnValidUpdate = new JButton("Validate");
		btnValidUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String firstName = user.getFirstName(), lastName = user.getLastName(), gender = user.getSexe(), oldPassword = null, newPassword = null;
				int age = user.getAge();
				if(text_FirstName.getText()!="")
					firstName = text_FirstName.getText();
				if(text_LastName.getText()!="")
					lastName = text_LastName.getText();
				if(!text_age.getText().equals("") && !text_age.getText().equals("Age")) {
					age = Integer.parseInt(text_age.getText());
				}
				if(text_Gender.getText()!="")
					gender = text_Gender.getText();
				if(text_Gender.getText()!="")
					oldPassword = text_OldPassword.getText();
				if(text_Gender.getText()!="")
					newPassword = text_NewPassword.getText();
				try {
					lblMessage.setText(user.updateUserData(user, firstName, lastName, age, gender, oldPassword, newPassword));
				} catch (SQLException e) {
					e.printStackTrace();
				}
				lblMessage.setVisible(true);
				text_FirstName.setEditable(false);
				text_FirstName.setText(user.getFirstName());
				text_LastName.setEditable(false);
				text_LastName.setText(user.getLastName());
				text_age.setEditable(false);
				text_age.setText(text_age.getText());
				text_Gender.setEditable(false);
				text_Gender.setText(user.getSexe());
				text_OldPassword.setVisible(false);
				text_NewPassword.setVisible(false);
				btnValidUpdate.setVisible(false);
				btnCancelUpdate.setVisible(false);
				lblOldPassword.setVisible(false);
				lblNewPassword.setVisible(false);
				text_Gender.setBounds(20, 264, 180, 36);
				text_age.setBounds(20, 217, 180, 36);
				text_LastName.setBounds(20, 170, 180, 36);
				text_FirstName.setBounds(20, 123, 180, 36);
				btnUpdate.setVisible(true);
				
			}
		});
		btnValidUpdate.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnValidUpdate.setBackground(Color.LIGHT_GRAY);
		btnValidUpdate.setBounds(15, 421, 91, 36);
		contentPane.add(btnValidUpdate);
		btnValidUpdate.setVisible(false);
		
		btnCancelUpdate = new JButton("Cancel");
		btnCancelUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdate.setVisible(true);
				
				text_FirstName.setEditable(false);
				text_FirstName.setText(user.getFirstName());
				text_LastName.setEditable(false);
				text_LastName.setText(user.getLastName());
				text_age.setEditable(false);
				text_age.setText(age);
				text_Gender.setEditable(false);
				text_Gender.setText(user.getSexe());
				text_OldPassword.setVisible(false);
				text_NewPassword.setVisible(false);
				btnValidUpdate.setVisible(false);
				btnCancelUpdate.setVisible(false);
				lblOldPassword.setVisible(false);
				lblNewPassword.setVisible(false);
			}
		});
		btnCancelUpdate.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnCancelUpdate.setBackground(Color.LIGHT_GRAY);
		btnCancelUpdate.setBounds(116, 421, 84, 36);
		contentPane.add(btnCancelUpdate);
		btnCancelUpdate.setVisible(false);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMessage.setVisible(false);
				text_FirstName.setEditable(true);
				text_LastName.setEditable(true);
				text_age.setEditable(true);
				text_Gender.setEditable(true);
				btnUpdate.setVisible(false);
				text_OldPassword.setVisible(true);
				text_NewPassword.setVisible(true);
				btnValidUpdate.setVisible(true);
				btnCancelUpdate.setVisible(true);
				lblOldPassword.setVisible(true);
				lblNewPassword.setVisible(true);
				text_LastName.setBounds(20, 134, 180, 36);
				text_FirstName.setBounds(20, 87, 180, 36);
				text_age.setBounds(20, 182, 180, 36);
				text_Gender.setBounds(20, 229, 180, 36);
				
			}
		});
		
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnUpdate.setBackground(Color.LIGHT_GRAY);
		btnUpdate.setBounds(20, 320, 180, 32);
		contentPane.add(btnUpdate);
		
		lblNewPassword = new JLabel("New password :");
		lblNewPassword.setForeground(Color.WHITE);
		lblNewPassword.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewPassword.setBounds(20, 276, 180, 14);
		contentPane.add(lblNewPassword);
		lblNewPassword.setVisible(false);
		
		lblOldPassword = new JLabel("Old password :");
		lblOldPassword.setForeground(Color.WHITE);
		lblOldPassword.setFont(new Font("Arial", Font.PLAIN, 18));
		lblOldPassword.setBounds(20, 348, 180, 14);
		contentPane.add(lblOldPassword);
		lblOldPassword.setVisible(false);
		
		lblMessage = new JLabel("");
		lblMessage.setVerticalAlignment(SwingConstants.TOP);
		lblMessage.setForeground(Color.WHITE);
		lblMessage.setFont(new Font("Arial", Font.PLAIN, 15));
		lblMessage.setBounds(20, 367, 220, 104);
		contentPane.add(lblMessage);
		lblMessage.setVisible(false);
		
		
	//FRIEND LIST
		
		
		nameFriends = user.getNameFriendList();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(711, 95, 214, 280);
		contentPane.add(scrollPane);
				
		listFriends = new JList(nameFriends.toArray());
		listFriends.setBounds(711, 96, 214, 278);
		scrollPane.setViewportView(listFriends);
		listFriends.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	            if (me.getClickCount() == 2) {
	            	String selected = (String) listFriends.getSelectedValue();
	            	String names[] = selected.split(" ");
	            	try {
						int userID = user.fetchUserID(names[0], names[1]);
						User friend = new User();
						friend = user.fetchUserData(userID);
						FriendUserWall friendUserWall = new FriendUserWall(friend,user);
						friendUserWall.setVisible(true);
					} catch (SQLException e) {
						
						e.printStackTrace();
					}
	            
	            }
	         }
	            });
		listFriends.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		
	//Button Friend Requests
		JButton btnFriendsRequest = new JButton("Friend requests");
		btnFriendsRequest.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnFriendsRequest.setBackground(Color.LIGHT_GRAY);
		btnFriendsRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				friendRequestsPanel.setVisible(true);
				achievement.setVisible(false);
				btnFriendsRequest.setEnabled(false);
		
			}
			
		});

		btnFriendsRequest.setBounds(711, 386, 214, 39);
		contentPane.add(btnFriendsRequest);
		
		JLabel lblFriendsList = new JLabel("Friends list :");
		lblFriendsList.setForeground(Color.WHITE);
		lblFriendsList.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 24));
		lblFriendsList.setBounds(711, 44, 214, 32);
		contentPane.add(lblFriendsList);
	//PANEL ACHIEVEMENT
		
		try {
			int idUser = user.getIdUser();
			achievement = new AchievementPanel(idUser);
			getContentPane().add(achievement);
			achievement.setBounds(239, 44, 441, 413);
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		
		
	//PANEL : FRIEND REQUESTS
		
		friendRequestsPanel = new JPanel();
		friendRequestsPanel.setBackground(Color.DARK_GRAY);
		friendRequestsPanel.setBounds(239, 44, 441, 413);
		contentPane.add(friendRequestsPanel);
		friendRequestsPanel.setLayout(null);
		friendRequestsPanel.setVisible(false);
		
		friendRequests = new ArrayList<>();
		usersSender = new HashMap<>();
		
		try {
			friendRequests = user.fetchFriendRequests();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		for(int iterator : friendRequests) {
			User userSender = new User();
			try {
				userSender = user.fetchUserData(iterator);
				usersSender.put(userSender.getFirstName()+" "+userSender.getLastName(),userSender.getIdUser());
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 108, 329, 237);
		friendRequestsPanel.add(scrollPane_1);
		listFriendRequests = new JList(usersSender.keySet().toArray());
		scrollPane_1.setViewportView(listFriendRequests);
		
		listFriendRequests.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	        	if(me.getClickCount() == 1) {
	        		String selected = (String) listFriendRequests.getSelectedValue();
	            	String names[] = selected.split(" ");
	            	try {
						userIDClicked = user.fetchUserID(names[0], names[1]);
					} catch (SQLException e) {
						e.printStackTrace();
					}
	            	btnAccept.addActionListener(new ActionListener() {
	        			public void actionPerformed(ActionEvent e) {
	        				if(!listFriendRequests.isSelectionEmpty()) {
	        					try {
									user.acceptFriendRequest(userIDClicked);
									UserWall userWall = new UserWall(user);
									userWall.setVisible(true);
									dispose();
						
									
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
	        				}
	        				
	        			}
	        		});
	        	}
	            if (me.getClickCount() == 2) {
	            	
	            	try {
						
						User friend = new User();
						friend = user.fetchUserData(userIDClicked);
						FriendUserWall friendUserWall = new FriendUserWall(friend,user);
						friendUserWall.setVisible(true);
					} catch (SQLException e) {
						
						e.printStackTrace();
					}
	            
	            }
	         }
	    });
		
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				friendRequestsPanel.setVisible(false);
				btnFriendsRequest.setEnabled(true);
				achievement.setVisible(true);
			}
		});
		btnClose.setFont(fontLittleButton);
		btnClose.setBackground(Color.LIGHT_GRAY);
		btnClose.setBounds(349, 294, 82, 39);
		friendRequestsPanel.add(btnClose);
		
		btnAccept= new JButton("Accept");
		btnAccept.setFont(fontLittleButton);
		btnAccept.setBackground(Color.LIGHT_GRAY);
		btnAccept.setBounds(349, 122, 82, 45);
		friendRequestsPanel.add(btnAccept);
	}
	
	//ACTION PERFORMED
	
	public void actionPerformed(ActionEvent arg0) {

		if(arg0.getSource()==mySportsButton) {
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==sportStatisticsButton) {
			SportStatistics sportStatistics;
			try {
				sportStatistics = new SportStatistics(user);
				sportStatistics.setVisible(true);
				this.dispose();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		if(arg0.getSource()==searchButton) {
			Search search = new Search(user);
			search.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==logoutButton) {
			Login login = new Login();
			login.setVisible(true);
			this.dispose();
		}	
	}
}
