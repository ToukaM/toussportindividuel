package ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import userData.User;
import javax.swing.JList;

public class Search extends JFrame implements ActionListener {

	private JPanel contentPane;
	private User user;
	private JButton userWallButton;
	private JButton mySportsButton;
	private JButton sportStatisticsButton;
	private JButton searchButton;
	private JButton logoutButton;
	private JTextField searchField;
	private JLabel searchLabel;
	private JButton startSearching;
	private JList<String> list;
	private JScrollPane scrollPane;
	private JLabel title;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		User user = new User();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Search frame = new Search(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Search(User user) {
		this.user = user;
		this.setSize(960, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		setResizable(false);
		
		Font fontButton = new Font("Microsoft YaHei UI Light", Font.BOLD, 18);
		
		/****************search field******************/
		
		title = new JLabel("Search a user");
		title.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 30));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setForeground(Color.WHITE);
		title.setBounds(90, 40, 290, 28);
		contentPane.add(title);
		
		searchLabel = new JLabel("Type the name you search here");
		searchLabel.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		searchLabel.setHorizontalAlignment(SwingConstants.CENTER);
		searchLabel.setForeground(Color.WHITE);
		searchLabel.setBounds(90, 110, 290, 28);
		contentPane.add(searchLabel);
		
		searchField = new JTextField();
		searchField.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		searchField.setHorizontalAlignment(SwingConstants.CENTER);
		searchField.setBounds(425, 100, 272, 48);
		contentPane.add(searchField);
		
		startSearching = new JButton("Search");
		startSearching.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 18));
		startSearching.setBackground(Color.GRAY);
		startSearching.setBounds(751, 106, 120, 37);
		contentPane.add(startSearching);
		startSearching.addActionListener(this);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(90, 200, 300, 247);
		contentPane.add(scrollPane);
		
		list = new JList<>(); 
		list.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		list.setFixedCellHeight(40);
		scrollPane.setViewportView(list);
		
		list.setFont(list.getFont().deriveFont(18f));
		
		
/*-------------navigation buttons-----------------*/
		
		userWallButton = new JButton("My Wall");
		userWallButton.setFont(fontButton);
		userWallButton.setBackground(Color.LIGHT_GRAY);
		userWallButton.setBounds(20, 500, 150, 45);
		contentPane.add(userWallButton);
		userWallButton.addActionListener(this);
		
		mySportsButton = new JButton("My Sports");
		mySportsButton.setFont(fontButton);
		mySportsButton.setBackground(Color.LIGHT_GRAY);
		mySportsButton.setBounds(190, 500, 150, 45);
		contentPane.add(mySportsButton);
		mySportsButton.addActionListener(this);
		
		sportStatisticsButton = new JButton("Sport Stats");
		sportStatisticsButton.setFont(fontButton);
		sportStatisticsButton.setBackground(Color.LIGHT_GRAY);
		sportStatisticsButton.setBounds(360, 500, 150, 45);
		contentPane.add(sportStatisticsButton);
		sportStatisticsButton.addActionListener(this);
		
		searchButton = new JButton("Search");
		searchButton.setFont(fontButton);
		searchButton.setBackground(Color.LIGHT_GRAY);
		searchButton.setBounds(530, 500, 150, 45);
		contentPane.add(searchButton);
		searchButton.setEnabled(false);
		
		logoutButton = new JButton("Logout");
		logoutButton.setFont(fontButton);
		logoutButton.setBackground(Color.LIGHT_GRAY);
		logoutButton.setBounds(817, 500, 116, 45);
		contentPane.add(logoutButton);
		logoutButton.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent arg0) {

		if(arg0.getSource()==mySportsButton) {
			MySports mySports = new MySports(user);
			mySports.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==sportStatisticsButton) {
			SportStatistics sportStatistics;
			try {
				sportStatistics = new SportStatistics(user);
				sportStatistics.setVisible(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.dispose();
		}
		if(arg0.getSource()==userWallButton) {
			UserWall userWall = new UserWall(user);   
			userWall.setVisible(true);
			this.dispose();
		}
		if(arg0.getSource()==startSearching) {
			ArrayList<Integer> users = new ArrayList<>();
			HashMap<String,Integer> usersSearched = new HashMap<>();
			String entry = searchField.getText();
			try {
				users = user.searchUser(entry);
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			for(int iterator : users) {
				User userSearched = new User();
				try {
					userSearched = user.fetchUserData(iterator);
					usersSearched.put(userSearched.getFirstName()+" "+userSearched.getLastName(),userSearched.getIdUser());
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
			list = new JList(usersSearched.keySet().toArray());
			scrollPane.setViewportView(list);
			list.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	            if (me.getClickCount() == 2) {
	            	String selected = (String) list.getSelectedValue();
	            	String names[] = selected.split(" ");
	            	try {
						int userID = user.fetchUserID(names[0], names[1]);
						User friend = new User();
						friend = user.fetchUserData(userID);
						FriendUserWall friendUserWall = new FriendUserWall(friend,user);
						friendUserWall.setVisible(true);
					} catch (SQLException e) {
						
						e.printStackTrace();
					}
	            
	            }
	         }
	            });
		}
		if(arg0.getSource()==logoutButton) {
			Login login = new Login();
			login.setVisible(true);
			this.dispose();
		}
	}
}
