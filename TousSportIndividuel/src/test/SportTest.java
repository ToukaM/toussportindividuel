package test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

import sportData.Bowling;
import sportData.Jogging;
import sportData.LongJump;
import sportData.ShotPut;
import sportData.Sport;
import sportData.Strengthening;
import sportData.Swimming;
import userData.User;

public class SportTest {

	/*@Test
	public void testInsert() throws SQLException {
		User user = new User(1,"touka","merouani",21,"femme","t_merouani@outlook.fr","1234",null,null,null);
		Bowling bowling = new Bowling();
		bowling.registerIntoDB(user, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 40,"2019-02-24");
		System.out.println("INSERT INTO BOWLING SUCCEDED");
		LongJump longJump = new LongJump();
		longJump.registerIntoDB(user, 2, 2,"2019-02-24");
		System.out.println("INSERT INTO LONGJUMP SUCCEDED");
		ShotPut shotPut = new ShotPut();
		shotPut.registerIntoDB(user, 2, 2,"2019-02-24");
		System.out.println("INSERT INTO SHOTPUT SUCCEDED");
		Strengthening strengthening = new Strengthening();
		strengthening.registerIntoDB(user, 2, "Abdos","2019-02-24");
		System.out.println("INSERT INTO STRENGHTENING SUCCEDED");
		Swimming swimming = new Swimming();
		swimming.registerIntoDB(user, 2, 2, "papillon","2019-02-24");
		System.out.println("INSERT INTO SWIMMING SUCCEDED");
	}*/
	
	@Test
	public void testFetch() throws SQLException{

		Bowling bowling = new Bowling();
		LongJump longJump = new LongJump();
		ShotPut shotPut = new ShotPut();
		Strengthening strengthening = new Strengthening();
		Swimming swimming = new Swimming();
		ArrayList<Bowling> bowlingList = new ArrayList<>();
		ArrayList<LongJump> longJumpList = new ArrayList<>();
		ArrayList<ShotPut> shotPutList = new ArrayList<>();
		ArrayList<Strengthening> strengtheningList = new ArrayList<>();
		ArrayList<Swimming> swimmingList = new ArrayList<>();
		bowlingList = bowling.fetchBowlingData(1);
		System.out.println(bowlingList);
		longJumpList = longJump.fetchLongJumpData(1);
		System.out.println(longJumpList);
		shotPutList = shotPut.fetchShotPutData(1);
		System.out.println(shotPutList);
		strengtheningList = strengthening.fetchStrengtheningData(1);
		System.out.println(strengtheningList);
		swimmingList = swimming.fetchSwimmingData(1);
		System.out.println(swimmingList);

		
		/*Iterator i = shotPutList.iterator();
		while(i.hasNext()){
		  ShotPut x = (ShotPut)i.next();
		  System.out.println(x.toString());
		}
		for(ShotPut swim : shotPutList) {
		System.out.println(swim.toString());
	
		}*/
		
	}
	
	/*@Test
	public void testUpdateData() throws SQLException {
		Bowling bowling = new Bowling();
		LongJump longJump = new LongJump();
		ShotPut shotPut = new ShotPut();
		Strengthening strengthening = new Strengthening();
		Swimming swimming = new Swimming();
		bowling.updateBowlingData(5, 1, 1, 1, 1, 10);
		longJump.updateLongJumpData(2, 1, 1);
		shotPut.updateShotPutData(2, 1, 1);
		strengthening.updateStrengtheningData(2, 1);
		swimming.updateSwimmingData(31, 1, 1);
	}*/
	
	/*@Test
	public void testDeleteData() throws SQLException {
		Bowling bowling = new Bowling();
		LongJump longJump = new LongJump();
		ShotPut shotPut = new ShotPut();
		Strengthening strengthening = new Strengthening();
		Swimming swimming = new Swimming();
		bowling.deleteBowlingData(5);
		longJump.deleteLongJumpData(2);
		shotPut.deleteShotPutData(2);
		strengthening.deleteStrengtheningData(2);
		swimming.deleteSwimmingData(31);
	}*/

}
