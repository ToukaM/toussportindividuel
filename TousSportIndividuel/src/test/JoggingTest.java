package test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

import sportData.Jogging;
import userData.User;
import sportData.Sport;

public class JoggingTest {

/*	@Test
	public void testRegisterIntoDB() throws SQLException {
		User user = new User(1,"touka","merouani",21,"femme","t_merouani@outlook.fr","1234",null,null,null);
		Jogging jogging = new Jogging();
		jogging.registerIntoDB(user, 70, 60, "2019-01-16");
		System.out.println("SUCCESS");
	}*/
	
	@Test
	public void testFetchJoggingData() throws SQLException {
		Jogging jogging = new Jogging();
		ArrayList<Jogging> data = new ArrayList<Jogging>();
		ArrayList<String> dates = new ArrayList<>();
		data = jogging.fetchJoggingData(21);
		System.out.println(data);
		int j = 0;
		for(Jogging jog : data) {
			String i = jog.fetchJoggingData(21).get(j).getDate();
			dates.add(i);
			j++;
		}
		System.out.println(dates);
	}
	
/*	@Test
	public void testDeleteJoggingData() throws SQLException {
		Jogging jogging = new Jogging();
		jogging.deleteJoggingData(3);
	}*/

	@Test
	public void testFetchDates() throws SQLException {
		Jogging jogging = new Jogging();
		System.out.println(jogging.fetchDates(21, "Jogging"));
		System.out.println(jogging.fetchJoggingDataByDate(21, "2019-01-28"));
	}
	
	/*@Test
	public void testUpdateData() throws SQLException {
		Jogging jogging = new Jogging();
		jogging.updateJoggingData(5, 5000, 50);
	}*/
}
