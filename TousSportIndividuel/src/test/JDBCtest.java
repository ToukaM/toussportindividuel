/**
 * 
 */
package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.junit.Test;

import com.mysql.jdbc.PreparedStatement;

import DataBaseOperations.DBConnection;

/**
 * @author Touka
 * This class is used to test the connectivity to the database
 */
public class JDBCtest {

	@Test
	public void test() {
		try {
	      Class.forName("com.mysql.jdbc.Driver");
	      System.out.println("Driver O.K.");

	      String url = "jdbc:mysql://mysql-projetintegration.alwaysdata.net:3306/projetintegration_2018";//jdbc:mysql+host+port+DBname
	      String user = "174903"; //DB user or superuser
	      String passwd = "cergyprojet"; //DB password

	      Connection conn = DriverManager.getConnection(url, user, passwd);
	      System.out.println("Connection succeded !");         
	      
	      /*****************TEST OF AN INSERT INTO*************/
	      
	      //PreparedStatement ps1 =(PreparedStatement) conn.prepareStatement("INSERT INTO User (firstName,lastName,age,sexe,mail,password) VALUES ('walid','tir',21,'homme','wtir@gmail.fr','1234')");
	      //int rs1 = ps1.executeUpdate();
	      
	      /****************Avec nom de colonne*****************/
	      
	      String mail = "sonia.laib@gmail.com";
	      PreparedStatement ps =(PreparedStatement) conn.prepareStatement("SELECT * FROM User WHERE mail='"+mail+"'");
	      ResultSet rs = ps.executeQuery();
	      
	      while(rs.next()){
	    	  System.out.println(rs.getInt("IdUser")); //Par nom de colonne
	      }
	      
	      /*******SANS CONNAITRE LES NOMS*******/
	      
	     System.out.println("\n SANS CONNAITRE LES NOMS de colonnes: \n");
	     
	      ResultSet rs2 = ps.executeQuery();
	      
	      
	      ResultSetMetaData rsmd2=rs2.getMetaData();
	      for(int i=1; i<=rsmd2.getColumnCount();i++){    //NOM DES COLONNES
	    	  System.out.print(rsmd2.getColumnName(i)+"\t");
	    	  
	      }
	      System.out.println("");
	      while(rs2.next()){
	    	  for(int i=1; i<=rsmd2.getColumnCount();i++){ 
	    		  System.out.print(rs2.getString(i)+"    \t");
	    	  }
	    	  System.out.println("");
	    	  System.out.println("le tableau ci-dessus n'est pas bien align�");
	      }
	    
		
	  	/********TEST SPORTS***********/
	     /*
			int id = 20;
			PreparedStatement psport =(PreparedStatement) conn.prepareStatement("SELECT * FROM Sports WHERE idUser='20'");
		      ResultSet rsport = psport.executeQuery();
		    	  
		    	  System.out.println(rsport.getInt("IdUser")); //Par nom de colonne
		   */
		    			int score = 0;
		    	
		    			PreparedStatement ps2 = (PreparedStatement) conn.prepareStatement("SELECT SUM(result) AS total_result FROM Bowling WHERE idUser = 21");
		    			ResultSet rs1 = ps2.executeQuery();
		    			rs1.next();
		    			System.out.println(rs1.getString(1)+" score total\n");
		    			score = Integer.parseInt(rs1.getString(1));
		    			System.out.println(score);
		    		

		      
		      
		} catch (Exception e) {
	      e.printStackTrace();
	    }
		
	
	}

}
