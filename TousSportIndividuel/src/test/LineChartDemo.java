/**
 * 
 */
package test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Touka
 *
 */
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import sportData.Bowling;
import sportData.LongJump;
import sportData.ShotPut;
import sportData.Strengthening;
import sportData.Swimming;

public class LineChartDemo extends ApplicationFrame {

	private static final long serialVersionUID = 1L;

	public LineChartDemo(String title) throws SQLException {
		super(title);
		XYDataset dataset = createDataset();
		JFreeChart chart = createChart(dataset);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		setContentPane(chartPanel);

	}

	private XYDataset createDataset() throws SQLException {
		Bowling bowling = new Bowling();
		LongJump longJump = new LongJump();
		ShotPut shotPut = new ShotPut();
		Strengthening strengthening = new Strengthening();
		Swimming swimming = new Swimming();
		ArrayList<Bowling> bowlingList = new ArrayList<>();
		ArrayList<LongJump> longJumpList = new ArrayList<>();
		ArrayList<ShotPut> shotPutList = new ArrayList<>();
		ArrayList<Strengthening> strengtheningList = new ArrayList<>();
		ArrayList<Swimming> swimmingList, swimmingList2 = new ArrayList<>();
		bowlingList = bowling.fetchBowlingData(1);
		System.out.println(bowlingList);
		longJumpList = longJump.fetchLongJumpData(1);
		System.out.println(longJumpList);
		shotPutList = shotPut.fetchShotPutData(1);
		System.out.println(shotPutList);
		strengtheningList = strengthening.fetchStrengtheningData(1);
		System.out.println(strengtheningList);
		
		
		swimmingList = swimming.fetchSwimmingData(21);
		System.out.println(swimmingList);
		
		swimmingList2 = swimming.fetchSwimmingData(1);
		System.out.println(swimmingList2);

		XYSeries series1 = new XYSeries("First");
		XYSeries series2 = new XYSeries("Second");
		int X=1;
		/*
		Iterator i = swimmingList.iterator();
		while(i.hasNext()){
		  String x = (String)i.next();
		  System.out.println(x);
		 }*/
		
		for(Swimming swim : swimmingList) {
			series1.add(X,swim.getDistanceSwimmed());
			System.out.println("\n"+X +" "+ swim.getDistanceSwimmed());
			X++;
		}
		X=1;
		for(Swimming swim : swimmingList2) {
			series2.add(X,swim.getDistanceSwimmed());
			System.out.println("\n"+X +" "+ swim.getDistanceSwimmed());
			X++;
		}
		

		XYSeries series3 = new XYSeries("Third");
		series3.add(1, 80);
		series3.add(2, 120);
		series3.add(3, 110);
		series3.add(4, 115);
		series3.add(5, 100);
		series3.add(6, 105);
		series3.add(7, 130);
		series3.add(8, 120);

		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(series1);
		dataset.addSeries(series2);
		dataset.addSeries(series3);

		return dataset;

	}

	private JFreeChart createChart(XYDataset dataset) {
		return ChartFactory.createXYLineChart("Line Chart Demo 6", "X", "Y", dataset, PlotOrientation.VERTICAL, true, true, false);
	}

	public static void main(String[] args) throws SQLException {
		LineChartDemo demo = new LineChartDemo("Line Chart Demo 6");
		demo.pack();
		RefineryUtilities.centerFrameOnScreen(demo);
		demo.setVisible(true);
	}

}
