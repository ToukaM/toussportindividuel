package test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import sportData.Sport;
import userData.User;

public class UserTest {

	/*@Test
	public void testUser() {
		User user = new User(0,"touka","merouani",21,"femme","t_merouani@outlook.fr","1234",null,null,null);
		System.out.println(user.toString());
		System.out.println("testUser : WORKS");
	}*/

	
	/**The registerIntoDB method works without verifying if the email already exists or not 
	 * so it would insert a user in the DB even if the given mail already exists
	 * that's why it is commented, so if you want to test the method please change the mail
	 * @throws SQLException
	 */
	/*@Test
	public void testRegisterIntoDB() throws SQLException {
		User user = new User(0,"touka","merouani",21,"femme","t_merouani@outlook.fr","1234",null,null);
		String salt = "saltvalue";
		user.registerIntoDB(salt);
		System.out.println(user.toString());
		System.out.println("testRegisterIntoDB : WORKS");
	}*/
	
	/*@Test
	public void testVerifyEmail() throws SQLException {
		User user = new User(0,"touka","merouani",21,"femme","t_merouani@outlook.fr","1234",null,null,null);
		boolean exist = user.verifyEmail("t_merouani@outlook.fr");
		System.out.println(exist);
		System.out.println("testVerifyEmail : WORKS");
		
	}
	
	@Test
	public void testLogin() throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());	
		System.out.println("testLogin : WORKS");
		
		System.out.println("TEST FRIENDSLIST :");
		ArrayList<Integer> friends = new ArrayList<Integer>();
		friends = user.getFriendList();
		System.out.println(friends.toString()+"\n");
		ArrayList<String> nameFriends = new ArrayList<String>();
		nameFriends = user.getNameFriendList();
		System.out.println(nameFriends.toString());
	}*/
	
	/**
	 * The following methods don't work unless a user is logged in
	 * No need to test fetchSport and fetchFriends methods because they are called
	 * automatically in the method Login
	 * @throws SQLException
	 */
	/*@Test
	public void testFetshSport() throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());
		user.fetchSports();
		System.out.println(user.toString());
	}
	
	@Test
	public void testFetshFriends() throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());
		user.fetchFriends();
		System.out.println(user.toString());
	}*/
	
	/*@Test
	public void testSendFriendRequest() throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());
		user.sendFriendRequest(1);
		System.out.println(user.toString());
	}
	
	@Test
	public void testAcceptFriendRequest() throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());
		user.acceptFriendRequest(19);
		System.out.println(user.toString());
	}
	
	@Test
	public void testFetchFriendRequests() throws SQLException {
		User user = new User();
		ArrayList<Integer> friendRequests = null;
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());
		friendRequests = user.fetchFriendRequests();
		System.out.println(friendRequests);
		
	}
	
	@Test 
	public void testAddSport() throws SQLException {
		User user = new User();
		user.login("sonia.laib@gmail.com","azerty");
		System.out.println(user.toString());
		user.addSport("Jogging");
		System.out.println(user.toString());
	}
	
	@Test
	public void testFetchUserData() throws SQLException {
		User actualUser = new User();
		actualUser.login("sonia.laib@gmail.com","azerty");
		System.out.println(actualUser.toString());
		User friendProfil = new User();
		friendProfil = actualUser.fetchUserData(21);
		System.out.println(friendProfil.toString());
		System.out.println(actualUser.toString());
	}
	@Test
	public void testString() {
		String name = "sALUT toi";
		name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
		System.out.println(name);
	}*/
	@Test
	public void testFetchFriendRequests() throws SQLException {
		User user = new User();
		user.login("gab","1234");
		ArrayList<Integer> usersFriendsRequests = new ArrayList<>();
		usersFriendsRequests = user.fetchFriendRequests();
		System.out.println(usersFriendsRequests);
		for(int i : usersFriendsRequests) {
			User j = user.fetchUserData(i);
			System.out.println(j);
		}
		
		System.out.println("Fin du testFetchFriendRequest \n\n");
	
	}
	/*
	@Test 
	public void testSearchUser() throws SQLException {
		User user = new User();
		ArrayList<Integer> users = new ArrayList<>();
		user.login("sonia.laib@gmail.com","azerty");
		users = user.searchUser("touka");
		System.out.println(users);
		for(int i : users) {
			User j = user.fetchUserData(i);
			System.out.println(j);
		}
	}
	
	@Test
	public void testFetchUserID() throws SQLException {
		User user = new User();
		int userID = user.fetchUserID("touka", "merouani");
		System.out.println(userID);
		boolean accepted = false ;
		accepted = user.verifyFriendShip(21, 13);
		System.out.println(accepted);
	}*/
	
	/*@Test 
	public void testAddToSportTable() throws SQLException {
		User user = new User();
		user.addToSportTable(19);
	}*/
}
