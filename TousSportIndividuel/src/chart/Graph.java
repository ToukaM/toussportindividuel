/**
 * 
 */
package chart;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Touka
 *
 */
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import sportData.Bowling;
import sportData.Jogging;
import sportData.LongJump;
import sportData.ShotPut;
import sportData.Strengthening;
import sportData.Swimming;

public class Graph {
	
	
	private Jogging jogging = new Jogging();
	private ArrayList<Jogging> joggingData = new ArrayList<>();
	private LongJump longJump = new LongJump();
	private ArrayList<LongJump> jumpData = new ArrayList<>();
	private ShotPut shotPut = new ShotPut();
	private ArrayList<ShotPut> shotData = new ArrayList<>();


	public Graph() throws SQLException{

	}
	
	public XYDataset createDatasetLineChart(String selectedSport, int idUser, int idFriend) throws SQLException {
		XYSeries series1 = null;
		int i = 1;
		int j = 1;
		switch (selectedSport) {
		case "Jogging" : joggingData = jogging.fetchJoggingData(idUser);
		series1 = new XYSeries("Me");
		for(Jogging iterator : joggingData) {
			series1.add(i, iterator.getDistance());
			i++;
			}
			break;
		case "LongJump" : jumpData = longJump.fetchLongJumpData(idUser);
		series1 = new XYSeries("Me");
		for(LongJump iterator : jumpData) {
			series1.add(i, iterator.getDistanceJumped());
			i++;
			}
			break;
		case "ShotPut" : shotData = shotPut.fetchShotPutData(idUser);
		series1 = new XYSeries("Me");
		for(ShotPut iterator : shotData) {
			series1.add(i, iterator.getDistance());
			i++;
			}
			break;
		}
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(series1);
		
		
		if(idFriend!=0) {
			XYSeries series2 = null;
			
			switch (selectedSport) {
			case "Jogging" : joggingData = jogging.fetchJoggingData(idFriend);
			series2 = new XYSeries("My friend");
			for(Jogging iterator : joggingData) {
				series2.add(j, iterator.getDistance());
				j++;
				}
				break;
			case "LongJump" : jumpData = longJump.fetchLongJumpData(idFriend);
			series2 = new XYSeries("My friend");
			for(LongJump iterator : jumpData) {
				series2.add(j, iterator.getDistanceJumped());
				j++;
				}
				break;
			case "ShotPut" : shotData = shotPut.fetchShotPutData(idFriend);
			series2 = new XYSeries("My friend");
			for(ShotPut iterator : shotData) {
				series2.add(j, iterator.getDistance());
				j++;
				}
				break;
			}
			dataset.addSeries(series2);
		}

		return dataset;

	}
	
	public CategoryDataset createDatasetBarChart(String selectedSport, int idUser, int friendId) throws SQLException {
		Bowling bowling = new Bowling();
		String series1 = "Splits";
		String series2 = "Open frames";
		String series3 = "Spares";
		String series4 = "Strikes";

		String category1 = "Me";
		String category2 = "My friend";

		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		dataset.addValue(bowling.totalSplits(idUser), series1, category1);
		dataset.addValue(bowling.totalOpenFrames(idUser), series2, category1);
		dataset.addValue(bowling.totalSpares(idUser), series3, category1);
		dataset.addValue(bowling.totalStrikes(idUser), series4, category1);
		
		if(friendId!=0) {
			dataset.addValue(bowling.totalSplits(friendId), series1, category2);
			dataset.addValue(bowling.totalOpenFrames(friendId), series2, category2);
			dataset.addValue(bowling.totalSpares(friendId), series3, category2);
			dataset.addValue(bowling.totalStrikes(friendId), series4, category2);
		}
		
		return dataset;

	}
	
	public PieDataset createDatasetPieChart(String selectedSport, int idUser, int idFriend) throws SQLException {
		DefaultPieDataset dataset = new DefaultPieDataset();
		
		switch (selectedSport) {
		case "Strengthening" : Strengthening strengthening = new Strengthening();
			dataset = new DefaultPieDataset();
			dataset.setValue("Push-up", strengthening.getTotalMovesNumber(idUser, "Push-up"));
			dataset.setValue("Abs", strengthening.getTotalMovesNumber(idUser, "Abs"));
			dataset.setValue("Squats", strengthening.getTotalMovesNumber(idUser, "Squats"));
			break;
		case "Swimming" : 
			Swimming swimming = new Swimming();
			dataset = new DefaultPieDataset();
			dataset.setValue("Crawl",swimming.getTotalDistanceSwimmed(idUser, "Crawl"));
			dataset.setValue("Butterfly",swimming.getTotalDistanceSwimmed(idUser, "Butterfly"));
			dataset.setValue("Breaststroke",swimming.getTotalDistanceSwimmed(idUser, "Breaststroke"));
			break;
		}
		
		if(idFriend!=0) {
			DefaultPieDataset friendDataset = new DefaultPieDataset();
			switch (selectedSport) {
			case "Strengthening" : Strengthening strengthening = new Strengthening();
				friendDataset.setValue("Friend: Push-up", strengthening.getTotalMovesNumber(idFriend, "Push-up"));
				friendDataset.setValue("Friend: Abs", strengthening.getTotalMovesNumber(idFriend, "Abs"));
				friendDataset.setValue("Friend: Squats", strengthening.getTotalMovesNumber(idFriend, "Squats"));
				break;
			case "Swimming" : 
				Swimming swimming = new Swimming();
				friendDataset.setValue("Friend: Crawl",swimming.getTotalDistanceSwimmed(idFriend, "Crawl"));
				friendDataset.setValue("Friend: Butterfly",swimming.getTotalDistanceSwimmed(idFriend, "Butterfly"));
				friendDataset.setValue("Friend: Breaststroke",swimming.getTotalDistanceSwimmed(idFriend, "Breaststroke"));
				break;
			}
		}
		return dataset;
	}
	
	
	public PieDataset createFriendDatasetPieChart(String selectedSport, int idFriend) throws SQLException {
		DefaultPieDataset friendDataset = new DefaultPieDataset();
		if(idFriend!=0) {
			
			switch (selectedSport) {
			case "Strengthening" : Strengthening strengthening = new Strengthening();
				friendDataset.setValue("Push-up", strengthening.getTotalMovesNumber(idFriend, "Push-up"));
				friendDataset.setValue("Abs", strengthening.getTotalMovesNumber(idFriend, "Abs"));
				friendDataset.setValue("Squats", strengthening.getTotalMovesNumber(idFriend, "Squats"));
				break;
			case "Swimming" : 
				Swimming swimming = new Swimming();
				friendDataset.setValue("Crawl",swimming.getTotalDistanceSwimmed(idFriend, "Crawl"));
				friendDataset.setValue("Butterfly",swimming.getTotalDistanceSwimmed(idFriend, "Butterfly"));
				friendDataset.setValue("Breaststroke",swimming.getTotalDistanceSwimmed(idFriend, "Breaststroke"));
				break;
			}
		}
		return friendDataset;
	}
	
	public JFreeChart createChart(String selectedSport, int idUser,int idFriend) throws SQLException {
		JFreeChart chart = null;
		XYDataset xydataset;
		CategoryDataset categorydataset;
		PieDataset piedataset;
		switch (selectedSport) {
		case "Jogging" : 
			xydataset = createDatasetLineChart(selectedSport, idUser, idFriend);
			return ChartFactory.createXYLineChart("Jogging line chart", "Sessions", "Distance", xydataset, PlotOrientation.VERTICAL, true, true, false);
		case "LongJump" : 
			xydataset = createDatasetLineChart(selectedSport, idUser, idFriend);
			return ChartFactory.createXYLineChart("Long Jump line chart", "Sessions", "Distance", xydataset, PlotOrientation.VERTICAL, true, true, false);
		case "ShotPut" : 
			xydataset = createDatasetLineChart(selectedSport, idUser, idFriend);
			return ChartFactory.createXYLineChart("Shot Put line chart", "Sessions", "Distance", xydataset, PlotOrientation.VERTICAL, true, true, false);
		case "Bowling" :
			categorydataset = createDatasetBarChart(selectedSport,idUser,idFriend);
			return ChartFactory.createBarChart("Bowling Bar Chart", "Item counting", "Total", categorydataset,
					PlotOrientation.VERTICAL, true, true, false);
		case "Swimming": 
			piedataset = createDatasetPieChart(selectedSport, idUser, idFriend);
			return ChartFactory.createPieChart("Swimming Pie Chart", piedataset, true, true, false);
		case "Strengthening": 
			piedataset = createDatasetPieChart(selectedSport, idUser, idFriend);
			return ChartFactory.createPieChart("Strengthening Pie Chart", piedataset, true, true, false);
		}
		return chart;
	}
	
	public JFreeChart createFriendChart(String selectedSport, int idFriend) throws SQLException {
		JFreeChart chart = null;
		PieDataset piedataset;
		switch (selectedSport) {
		case "Swimming": 
			piedataset = createFriendDatasetPieChart(selectedSport, idFriend);
			return ChartFactory.createPieChart("Friend Swimming Pie Chart", piedataset, true, true, false);
		case "Strengthening": 
			piedataset = createFriendDatasetPieChart(selectedSport, idFriend);
			return ChartFactory.createPieChart("Friend Strengthening Pie Chart", piedataset, true, true, false);
		}
		return chart;
	}
	
	public void saveChart(JFreeChart chart, String path) {
		path = path + ".png";
		File imageFile = new File(path);
		int width = 640;
		int height = 480;
		try {
		    ChartUtilities.saveChartAsPNG(imageFile, chart, width, height);
		} catch (IOException ex) {
		    System.err.println(ex);
		}
	}


}
